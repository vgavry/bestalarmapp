package com.iv.bestalarmapp.ui;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.gc.materialdesign.views.ButtonFlat;
import com.iv.bestalarmapp.ui.model.Alarm;
import com.iv.bestalarmapp.ui.service.AlarmService;
import com.iv.bestalarmapp.ui.service.IntentStorage;
import com.iv.bestalarmapp.ui.service.VibrationService;

public class EditAlarmActivity extends FragmentActivity implements
		OnClickListener {

	private PagerSlidingTabStrip tabs;
	private ViewPager pager;
	private EditAlarmPagerAdapter adapter;
	private AlarmService alarmService;
	private IntentStorage activityStorge;
	private VibrationService vibrationService;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_alarm);
		alarmService = new AlarmService(this);
		activityStorge = new IntentStorage(this);
		vibrationService = new VibrationService(this);
		
		TextView title = (TextView) findViewById(R.id.textTitle);
		tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
		pager = (ViewPager) findViewById(R.id.pager);
		adapter = new EditAlarmPagerAdapter(this, getSupportFragmentManager());
		pager.setAdapter(adapter);
		final int pageMargin = (int) TypedValue.applyDimension(
				TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
						.getDisplayMetrics());
		pager.setPageMargin(pageMargin);
		tabs.setViewPager(pager);
		ButtonFlat buttonSaveAlarm = (ButtonFlat) findViewById(R.id.buttonSaveAlarm);
		buttonSaveAlarm.setOnClickListener(this);
		
		Alarm alarm = activityStorge.get(Alarm.class);
		if (alarm.id>0){
			title.setText(R.string.EditAlarmTitle);
		}else{
			title.setText(R.string.NewAlarmTitle);
		}
	}

	public void onCloseButton(View view) {
		finish();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		alarmService.destroy();
		vibrationService.stop();
	}

	@Override
	public void onClick(View v) {
		Alarm alarm = activityStorge.get(Alarm.class);
		alarmService.save(alarm);
		finish();
	}
	
	
}
