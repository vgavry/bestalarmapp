package com.iv.bestalarmapp.ui;

import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.iv.bestalarmapp.ui.model.Alarm;
import com.iv.bestalarmapp.ui.model.Vibration;
import com.iv.bestalarmapp.ui.service.IntentStorage;
import com.iv.bestalarmapp.ui.service.VibrationService;

public class ChooseVibrateFragment extends Fragment implements OnItemClickListener {

	public ChooseVibrateFragment() {
		super();
	}

	private ListView listView;
	private ListAdapter<Vibration> adapter;
	private IntentStorage storage;
	private VibrationService vibrationService;
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		storage = new IntentStorage(getActivity());
		vibrationService = new VibrationService(getActivity());
	}
	
	@Override
	public void onResume() {
		super.onResume();
		Alarm alarm = storage.get(Alarm.class);
		List<Vibration> vibrations = vibrationService.getVibrations();
		adapter.clear();
		adapter.addAll(vibrations);
		adapter.setSelectedItemPosition(alarm.vibrationId);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_vibrations, container, false);
		listView = (ListView) v.findViewById(R.id.listView);
		adapter = new VibrationListAdapter(getActivity());
		listView.setItemsCanFocus(false);
		listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		listView.setAdapter(adapter);
		listView.setDividerHeight(0);
		listView.setOnItemClickListener(this);
		return v;

	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		vibrationService.stop();
	}


	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int index, long arg3) {
		adapter.setSelectedItemPosition(index);
	}

}
