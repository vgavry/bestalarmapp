package com.iv.bestalarmapp.ui;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.nostra13.universalimageloader.core.assist.FailReason;

public class ProgressImageLoadingListener implements AlarmImageLoadingListener {

	private ImageView view;

	public ProgressImageLoadingListener(ImageView view) {
		this.view = view;
	}

	@Override
	public void onLoadingCancelled(String arg0, View arg1) {
		
		view.setScaleType(ScaleType.CENTER);
	}

	@Override
	public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
		view.setScaleType(ScaleType.CENTER_CROP);
		//this.pieView.setProgress(100);
	}

	@Override
	public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
		view.setScaleType(ScaleType.CENTER);
	}

	@Override
	public void onLoadingStarted(String arg0, View arg1) {
		view.setScaleType(ScaleType.CENTER);
		view.setImageResource(R.drawable.loading1);
	}

	@Override
	public void onProgressUpdate(String arg0, View arg1, int arg2, int arg3) {
		
	}

}
