package com.iv.bestalarmapp.ui;

import com.iv.bestalarmapp.ui.model.Alarm;
import com.iv.bestalarmapp.ui.model.Image;
import com.iv.bestalarmapp.ui.model.Resource;
import com.iv.bestalarmapp.ui.model.SourceType;

public class ChooseImageFragment extends AbstractChooseResourceFragment<Image> {

	public ChooseImageFragment() {
		super();
	}

	protected Class<ChooseImageActivity> getEditResourceActivity() {
		return ChooseImageActivity.class;
	}

	protected Image getResource(Alarm alarm) {
		return alarm.image;
	}
	
	protected int getLayoutResourceId() {
		return R.layout.fragment_image;
	}

	@Override
	protected SourceType getSourceType() {
		return SourceType.IMAGE;
	}

	@Override
	protected String getImageUri(Alarm alarm) {
		return alarm.image.uri;
	}

	@Override
	protected String getFormattedResourceInfo(String mediaSourceName,
			Image resource) {
		StringBuilder builder = new StringBuilder();
		if (resource.isRandom){
			builder.append("Random image \r\n");
		}else{
			builder.append("Image ");
		}
		builder.append("from '");
		builder.append(mediaSourceName);
		builder.append("'");
		
		if (resource.filter!=null && !resource.filter.isEmpty()){
			builder.append("\r\nby filter '");
			builder.append(resource.filter);
			builder.append("'");
		}
		return builder.toString();
	}
}
