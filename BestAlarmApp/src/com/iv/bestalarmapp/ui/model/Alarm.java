package com.iv.bestalarmapp.ui.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Alarm implements Serializable {

	@DatabaseField(generatedId = true)
	public int id;

	@DatabaseField(canBeNull = true, foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
	public Image image;

	@DatabaseField(canBeNull = true, foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
	public Sound sound;

	@DatabaseField
	public int vibrationId;

	@DatabaseField
	public String text;

	@DatabaseField
	public int hour = 12;

	@DatabaseField
	public int minute = 0;

	@DatabaseField
	public boolean isAm;

	@ForeignCollectionField(eager = true)
	public Collection<Day> days = new ArrayList<Day>();

	@DatabaseField
	public boolean isActive=true;

}
