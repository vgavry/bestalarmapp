package com.iv.bestalarmapp.ui.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Image implements Resource<ImageUri> {

	@DatabaseField(generatedId = true)
	private Integer id;

	@DatabaseField
	public String uri;

	@DatabaseField
	public String filter;

	@DatabaseField
	public boolean isRandom;
	
	@DatabaseField
	public int mediaSourceId;

	@DatabaseField
	public boolean isRandomUriLoaded;
	
	@ForeignCollectionField(eager = true)
	public Collection<ImageUri> randomUrisHistory = new ArrayList<ImageUri>();
	
	
	public Image() {
		super();
	}

	public Image(String uri) {
		this.uri = uri;
	}

	@Override
	public String getUri() {
		return uri;
	}

	@Override
	public String getFilter() {
		return filter;
	}

	@Override
	public boolean isRandom() {
		return isRandom;
	}

	public void setFilter(String filter2) {
		this.filter = filter2;
	}

	public int getMediaSourceId() {
		return mediaSourceId;
	}

	public void setMediaSourceId(int mediaSourceId) {
		this.mediaSourceId = mediaSourceId;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public void setRandom(boolean isRandom) {
		this.isRandom = isRandom;
	}

	@Override
	public boolean isRandomUriLoaded() {
		return isRandomUriLoaded;
	}

	@Override
	public void setRandomUriLoaded(boolean b) {
		isRandomUriLoaded = b;
		
	}

	@Override
	public Collection<ImageUri> getRandomUrisHistory() {
		return randomUrisHistory;
	}

	
	

}
