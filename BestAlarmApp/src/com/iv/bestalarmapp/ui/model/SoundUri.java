package com.iv.bestalarmapp.ui.model;

import java.io.Serializable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class SoundUri implements Serializable,ResourceUri<Sound> {

	public SoundUri() {
		super();
	}

	public SoundUri(String uri) {
		this.uri = uri;
	}

	@DatabaseField(generatedId = true)
	private Integer id;

	@DatabaseField
	public String uri;

	@DatabaseField(foreign = true, foreignAutoRefresh = true)
	public Sound image;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uri == null) ? 0 : uri.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SoundUri other = (SoundUri) obj;
		if (uri == null) {
			if (other.uri != null)
				return false;
		} else if (!uri.equals(other.uri))
			return false;
		return true;
	}

	@Override
	public void setResource(Sound resource) {
		this.image = resource;
	}
	
	
}
