package com.iv.bestalarmapp.ui.model;

import java.io.Serializable;
import java.util.Collection;



public interface Resource<T extends ResourceUri>  extends Serializable {

	String getUri();

	String getFilter();

	boolean isRandom();

	void setFilter(String filter);

	void setRandom(boolean random);

	void setMediaSourceId(int id);

	void setUri(String uri);

	int getMediaSourceId();

	boolean isRandomUriLoaded();

	void setRandomUriLoaded(boolean b);
	
	Collection<T> getRandomUrisHistory();
	
	
	
}
