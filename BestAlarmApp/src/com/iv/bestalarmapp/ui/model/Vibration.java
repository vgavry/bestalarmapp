package com.iv.bestalarmapp.ui.model;

public class Vibration {

	private String name;

	private long[] pattern;

	private Integer id;

	public Vibration() {
		super();
	}

	public Vibration(String name, long[] pattern) {
		super();
		this.name = name;
		this.pattern = pattern;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long[] getPattern() {
		return pattern;
	}

	public void setPattern(long[] pattern) {
		this.pattern = pattern;
	}

}
