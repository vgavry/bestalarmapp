package com.iv.bestalarmapp.ui.model;

import java.util.ArrayList;
import java.util.Collection;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Sound implements Resource<SoundUri> {

	@DatabaseField(generatedId = true)
	private Integer id;

	@DatabaseField
	public String uri;

	@DatabaseField
	public String filter;

	@DatabaseField
	public boolean isRandom;
	
	@DatabaseField
	public boolean isRandomUriLoaded;
	
	@ForeignCollectionField(eager = true)
	public Collection<SoundUri> randomUrisHistory = new ArrayList<SoundUri>();
	
	@DatabaseField
	public String onlineUri;
	
	public boolean getRandom() {
		return isRandom;
	}

	public void setRandom(boolean isRandom) {
		this.isRandom = isRandom;
	}

	public int getMediaSourceId() {
		return mediaSourceId;
	}

	public void setMediaSourceId(int mediaSourceId) {
		this.mediaSourceId = mediaSourceId;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	@DatabaseField
	public int mediaSourceId = 100;

	@DatabaseField
	public String title;

	@DatabaseField
	public String imageUri;

	@DatabaseField
	public boolean isRingtone;

	@DatabaseField
	public String ringtoneId;

	public Sound(String uri) {
		this.uri = uri;
	}

	public Sound() {
		super();
	}

	@Override
	public String getUri() {
		return uri;
	}

	@Override
	public String getFilter() {
		return filter;
	}

	@Override
	public boolean isRandom() {
		return isRandom;
	}

	@Override
	public boolean isRandomUriLoaded() {
		return isRandomUriLoaded;
	}

	@Override
	public void setRandomUriLoaded(boolean b) {
		isRandomUriLoaded = b;
	}

	@Override
	public Collection<SoundUri> getRandomUrisHistory() {
		return randomUrisHistory;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uri == null) ? 0 : uri.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sound other = (Sound) obj;
		if (uri == null) {
			if (other.uri != null)
				return false;
		} else if (!uri.equals(other.uri))
			return false;
		return true;
	}
	
	


}
