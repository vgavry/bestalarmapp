package com.iv.bestalarmapp.ui.model;

import java.io.Serializable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class ImageUri implements Serializable,ResourceUri<Image> {

	public ImageUri() {
		super();
	}

	public ImageUri(String uri) {
		this.uri = uri;
	}

	@DatabaseField(generatedId = true)
	private Integer id;

	@DatabaseField
	public String uri;

	@DatabaseField(foreign = true, foreignAutoRefresh = true)
	public Image image;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uri == null) ? 0 : uri.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImageUri other = (ImageUri) obj;
		if (uri == null) {
			if (other.uri != null)
				return false;
		} else if (!uri.equals(other.uri))
			return false;
		return true;
	}

	@Override
	public void setResource(Image resource) {
		this.image = resource;
	}
	
	
}
