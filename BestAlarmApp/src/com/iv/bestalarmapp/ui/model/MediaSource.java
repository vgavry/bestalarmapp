package com.iv.bestalarmapp.ui.model;

import java.util.List;

import android.content.Context;

public interface MediaSource<T extends Resource> {

	SourceType getType();

	int getId();

	String getName();

	List<T> load(Context context, String filter);
	
	public void clear();
	
	public boolean isOnline();

	
	
}
