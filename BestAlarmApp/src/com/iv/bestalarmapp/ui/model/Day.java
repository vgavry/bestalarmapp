package com.iv.bestalarmapp.ui.model;

import java.io.Serializable;
import java.util.Calendar;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Day implements Serializable, Comparable<Day> {

	@DatabaseField(generatedId = true)
	public int id;

	@DatabaseField(foreign = true, foreignAutoRefresh = true)
	public Alarm alarm;

	@DatabaseField
	public int type = Calendar.DAY_OF_WEEK;

	@DatabaseField
	public int dayOfWeek;

	@DatabaseField
	public long day;
	
	@DatabaseField
	public boolean selected;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (day ^ (day >>> 32));
		result = prime * result + dayOfWeek;
		result = prime * result + type;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Day other = (Day) obj;
		if (day != other.day)
			return false;
		if (dayOfWeek != other.dayOfWeek)
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	@Override
	public int compareTo(Day another) {
		int diff = new Integer(type).compareTo(new Integer(another.type));
		if (diff == 0) {
			diff = new Integer(dayOfWeek).compareTo(new Integer(
					another.dayOfWeek));
		}
		if (diff == 0) {
			diff = new Long(day).compareTo(new Long(another.day));
		}
		return diff;
	}

}
