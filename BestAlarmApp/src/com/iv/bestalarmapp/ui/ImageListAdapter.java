package com.iv.bestalarmapp.ui;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.gc.materialdesign.views.CheckBox;
import com.gc.materialdesign.views.CheckBox.OnCheckListener;
import com.iv.bestalarmapp.ui.model.Image;
import com.iv.bestalarmapp.ui.service.ImageService;

public class ImageListAdapter extends ListAdapter<Image> implements
		OnClickListener, OnCheckListener {

	private AbstractResourceActivity activity;

	public ImageListAdapter(AbstractResourceActivity context) {
		super(context, R.layout.item_image, R.id.image, R.id.rippleView,
				R.id.imageContainer, R.id.checkBox);
		activity = context;
	}

	@Override
	protected void formatElement(Integer integer, View view, Image object,
			View parenView, int position, boolean checked) {

		switch (integer) {

		case R.id.image:
			ImageView imageView = (ImageView) view;
			ImageService.loadImage(context, imageView, object.uri,
					new ProgressImageLoadingListener(imageView));
			break;
		case R.id.rippleView:
			view.setTag(R.id.rippleView, object);
			view.setTag(R.id.image, parenView.findViewById(R.id.image));
			view.setOnClickListener(this);
			break;

		case R.id.checkBox:
			CheckBox box = (CheckBox) view;
			box.setChecked(checked);
			view.setTag(R.id.rippleView, position);
			box.setOncheckListener(this);
			break;

		case R.id.imageContainer:
			if (checked) {
				view.setBackgroundResource(R.drawable.selected_view);
			} else {
				view.setBackgroundResource(R.drawable.rounded_view);
			}
			break;

		default:
			break;
		}

	}

	@Override
	public void onClick(View v) {
		Image image = (Image) v.getTag(R.id.rippleView);
		View imageView = (View) v.getTag(R.id.image);
		showFullScreenView(imageView, image);
	}

	private void showFullScreenView(View v, Image image) {
		if (activity instanceof Foldable) {
			Foldable foldable = (Foldable) activity;
			foldable.openView(v, image.uri);
		}
	}

	@Override
	public void onCheck(CheckBox checkBox, boolean check) {
		Integer position = (Integer) checkBox.getTag(R.id.rippleView);
		setSelectedItemPosition(position);
	}

}
