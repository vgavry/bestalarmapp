package com.iv.bestalarmapp.ui;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gc.materialdesign.views.CheckBox;
import com.gc.materialdesign.views.CheckBox.OnCheckListener;
import com.iv.bestalarmapp.ui.model.Alarm;
import com.iv.bestalarmapp.ui.model.Vibration;
import com.iv.bestalarmapp.ui.service.IntentStorage;
import com.iv.bestalarmapp.ui.service.VibrationService;

public class VibrationListAdapter extends ListAdapter<Vibration> implements
		OnCheckListener, OnClickListener {

	private IntentStorage storage;
	private VibrationService vibrationService;

	public VibrationListAdapter(Activity context) {
		super(context, R.layout.item_vibration_choice, R.id.text1,
				R.id.checkBox, R.id.imageButtonPlay);
		storage = new IntentStorage(context);
		vibrationService = new VibrationService(this.context);
	}

	@Override
	protected void formatElement(Integer integer, View view, Vibration object,
			View parenView, int position, boolean checked) {

		switch (integer) {
		case R.id.text1:
			TextView textView = (TextView) view;
			textView.setText(object.getName());
			break;
		case R.id.checkBox:
			CheckBox checkBox = (CheckBox) view;
			checkBox.setTag(position);
			checkBox.setOncheckListener(this);
			if (!checkBox.isCheck() && checked) {
				checkBox.setChecked(checked);
			} else if (!checked) {
				checkBox.setChecked(false);
			}
			break;

		case R.id.imageButtonPlay:
			ImageButton button = (ImageButton) view;
			button.setTag(R.id.imageButtonPlay, position);
			button.setOnClickListener(this);
			if (vibrationService.isPlaying(position)) {
				button.setImageResource(R.drawable.ic_pause_circle_outline_black_36dp);
			} else {
				button.setImageResource(R.drawable.ic_play_circle_outline_black_36dp);
			}
			break;

		default:
			break;
		}

	}

	@Override
	public void onCheck(CheckBox checkBox, boolean check) {
		Integer index = (Integer) checkBox.getTag();
		if (index != null) {
			setSelectedItemPosition(index);
		}

	}

	@Override
	public void setSelectedItemPosition(int checked) {
		Alarm alarm = storage.get(Alarm.class);
		alarm.vibrationId = checked;
		storage.put(alarm);
		super.setSelectedItemPosition(checked);
	}

	@Override
	public void onClick(View v) {
		Integer pos = (Integer) v.getTag(R.id.imageButtonPlay);
		vibrationService.play(pos);
		notifyDataSetChanged();
	}

}
