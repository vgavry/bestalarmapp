package com.iv.bestalarmapp.ui;

import com.android.datetimepicker.time.RadialPickerLayout;
import com.android.datetimepicker.time.TimePickerDialog;
import com.android.datetimepicker.time.TimePickerDialog.OnTimeSetListener;
import com.iv.bestalarmapp.ui.model.Alarm;
import com.iv.bestalarmapp.ui.service.IntentStorage;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class EditAlarmPagerAdapter extends FragmentPagerAdapter implements
		OnTimeSetListener {

	private final String[] TITLES = { "Time", "Days", "Image", "Sound",
			"Vibrate" };

	private Activity activity;

	private IntentStorage activityStorage;

	public EditAlarmPagerAdapter(Activity alarmEditor, FragmentManager fm) {
		super(fm);
		this.activity = alarmEditor;
		activityStorage = new IntentStorage(alarmEditor);
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return TITLES[position];
	}

	@Override
	public int getCount() {
		return TITLES.length;
	}

	@Override
	public Fragment getItem(int position) {
		Fragment fragment = null;
		switch (position) {
		case 0:
			fragment = createTimeFragment();
			break;
		case 1:
			fragment = new ChooseDaysFragment();
			break;
		case 2:
			fragment = new ChooseImageFragment();
			break;
		case 3:
			fragment = new ChooseSoundFragment();
			break;
		case 4:
			fragment = new ChooseVibrateFragment();
			break;

		}
		return fragment;
	}

	private Fragment createTimeFragment() {
		Alarm alarm = activityStorage.get(Alarm.class);
		final TimePickerDialog ret = new TimePickerDialog(){
			@Override
			public void onResume() {
				super.onResume();
				Alarm alarm = activityStorage.get(Alarm.class);
				setTime(alarm.hour,alarm.minute);
			}
		};
		ret.initialize(this, alarm.hour, alarm.minute, true);
		ret.setOnTimeSetListener(this);
		return ret;
	}

	@Override
	public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
		Alarm alarm = activityStorage.get(Alarm.class);
		alarm.hour = hourOfDay;
		alarm.minute = minute;
		activityStorage.put(alarm);

	}
}