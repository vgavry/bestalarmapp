package com.iv.bestalarmapp.ui;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.gc.materialdesign.views.ButtonRectangle;
import com.iv.bestalarmapp.ui.model.Alarm;
import com.iv.bestalarmapp.ui.service.AlarmService;
import com.iv.bestalarmapp.ui.service.ImageService;
import com.iv.bestalarmapp.ui.service.IntentStorage;
import com.iv.bestalarmapp.ui.service.RandomImageService;
import com.iv.bestalarmapp.ui.service.RandomResourceService;
import com.iv.bestalarmapp.ui.service.RandomSoundService;
import com.iv.bestalarmapp.ui.service.SoundService;
import com.iv.bestalarmapp.ui.service.VibrationService;

public class AlarmActivity extends Activity implements OnClickListener, Runnable {

	private IntentStorage storage;
	private ImageView imageView;
	private VibrationService vibrationService;
	private Handler mHandler;
	private SoundService soundService;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_alarm);
		storage = new IntentStorage(this);
		vibrationService = new VibrationService(this);
		soundService = new SoundService(this,new AlarmService(this));
		imageView = (ImageView) findViewById(R.id.image);
		ButtonRectangle rectangle = (ButtonRectangle) findViewById(R.id.buttonStop);
		rectangle.setOnClickListener(this);
		mHandler = new Handler();
		mHandler.postDelayed(this, 15*60*1000);
	}

	@Override
	protected void onResume() {
		super.onResume();
		Alarm alarm = storage.get(Alarm.class);
		if (alarm.image != null) {
			ImageService.loadImage(this, imageView, alarm.image.uri, null);
		} else {
			ImageService.loadDefaultImage(this, imageView);
		}
		soundService.play(alarm.sound);
		vibrationService.play(alarm.vibrationId);
	}

	@Override
	public void onClick(View v) {
		finish();
		vibrationService.stop();
		soundService.stop();
	}
	
	@Override
	protected void onDestroy() {
		vibrationService.stop();
		soundService.stop();
		
		Alarm alarm = storage.get(Alarm.class);
		AlarmService alarmService = new AlarmService(this);
		RandomImageService imageService = new RandomImageService(this,alarmService);
		imageService.reload(alarm.image);
		RandomSoundService randomSoundService = new RandomSoundService(this, alarmService);
		randomSoundService.reload(alarm.sound);
		
		super.onDestroy();
	}

	@Override
	public void run() {
		vibrationService.stop();
		soundService.stop();
	}
}
