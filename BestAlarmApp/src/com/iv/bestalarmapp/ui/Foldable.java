package com.iv.bestalarmapp.ui;

import android.view.View;

public interface Foldable {

	void openView(View v, String uri);

}
