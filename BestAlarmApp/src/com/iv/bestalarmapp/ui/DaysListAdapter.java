package com.iv.bestalarmapp.ui;

import java.util.Calendar;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.TextView;

import com.gc.materialdesign.views.CheckBox;
import com.gc.materialdesign.views.CheckBox.OnCheckListener;
import com.iv.bestalarmapp.ui.model.Day;

public class DaysListAdapter extends ListAdapter<Day> implements
		OnCheckListener {

	private String[] daysOfWeekNames;

	private java.text.DateFormat dateFormat;

	private ChooseDaysFragment fragment;

	public DaysListAdapter(Context context, ChooseDaysFragment fragment,
			int layoutItemResourceId, int... itemResourcesIds) {
		super(context, layoutItemResourceId, itemResourcesIds);
		this.fragment = fragment;
		daysOfWeekNames = context.getResources().getStringArray(R.array.days);
		dateFormat = DateFormat.getDateFormat(context);
	}

	@Override
	protected void formatElement(Integer integer, View view, Day object,
			View parenView, int position, boolean checked) {

		switch (integer) {
		case R.id.text1:
			TextView textView = (TextView) view;
			if (object.type == Calendar.DAY_OF_WEEK) {
				textView.setText(daysOfWeekNames[object.dayOfWeek - 1]);
			} else {
				Calendar calendar = Calendar.getInstance();
				calendar.setTimeInMillis(object.day);
				String text = dateFormat.format(calendar.getTime());
				textView.setText(text);
			}
			break;
		case R.id.checkBox:
			CheckBox checkBox = (CheckBox) view;
			checkBox.setTag(object);
			checkBox.setOncheckListener(this);
			if (!checkBox.isCheck() && object.selected) {
				checkBox.setChecked(true);
			} else if (!object.selected) {
				checkBox.setChecked(false);
			}
			break;

		default:
			break;
		}

	}

	@Override
	public void onCheck(CheckBox checkBox, boolean check) {
		Day index = (Day) checkBox.getTag();
		fragment.saveElement(index);
	}

}
