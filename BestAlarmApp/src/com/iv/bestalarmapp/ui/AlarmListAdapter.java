package com.iv.bestalarmapp.ui;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.text.format.DateFormat;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.gc.materialdesign.views.CheckBox;
import com.gc.materialdesign.views.CheckBox.OnCheckListener;
import com.iv.bestalarmapp.ui.model.Alarm;
import com.iv.bestalarmapp.ui.model.Day;
import com.iv.bestalarmapp.ui.service.AlarmService;
import com.iv.bestalarmapp.ui.service.ImageService;
import com.iv.bestalarmapp.ui.service.IntentStorage;

public class AlarmListAdapter extends ListAdapter<Alarm> implements OnCheckListener {

	private IntentStorage activityStorage;

	private String[] daysOfWeekNames;

	private java.text.DateFormat dateFormat;

	private AlarmService alarmService;

	public AlarmListAdapter(Activity context) {
		super(context, R.layout.item_alarm_portraite, R.id.text,
				R.id.rippleView, R.id.textDays, R.id.image, R.id.checkBox,
				R.id.textElapsed,R.id.buttonAlarmDelete);
		activityStorage = new IntentStorage(context);
		daysOfWeekNames = context.getResources().getStringArray(
				R.array.days_short);
		dateFormat = DateFormat.getDateFormat(context);
		alarmService = new AlarmService(context);
	}

	@Override
	protected void formatElement(Integer integer, View view, Alarm object,
			View parenView, int position, boolean checked) {

		switch (integer) {
		case R.id.text:
			TextView textView = (TextView) view;
			String time = String.format("%d:%02d", object.hour, object.minute);
			textView.setText(time);
			break;
		case R.id.textDays:
			formatDays(view, object);
			break;

		case R.id.checkBox:
			CheckBox checkBox = (CheckBox) view;
			checkBox.setChecked(object.isActive);
			checkBox.setTag(R.id.rippleView, object);
			checkBox.setTag(R.id.checkBox, position);
			checkBox.setOncheckListener(this);
			break;
			
		case R.id.buttonAlarmDelete:
			view.setTag(R.id.buttonAlarmDelete, position);
			//view.setOnClickListener(this);
			break;

		case R.id.textElapsed:
			TextView elapsedTextView = (TextView) view;
			formatElapsedTime(elapsedTextView, object);
			break;
		case R.id.image:
			ImageView imageView = (ImageView) view;
			if (object.image != null) {
				ImageService.loadImage(context, imageView, object.image.uri,
						null);
			} else {
				ImageService.loadImage(context, imageView, null, null);
			}
		default:
			break;
		}

	}

	private void formatElapsedTime(TextView elapsedTextView, Alarm object) {
		if (object.isActive) {
			doFormatElapsedTime(elapsedTextView, object);
		} else {
			elapsedTextView.setText("");
		}
	}

	private void doFormatElapsedTime(TextView elapsedTextView, Alarm alarm) {
		Collection<Day> days = alarm.days;
		long currentTime = new Date().getTime();
		long minDiff = Long.MAX_VALUE;
		for (Day day : days) {
			Calendar calendar = createCalendar(alarm, day);
			if (calendar != null && day.selected) {
				long diff = calendar.getTimeInMillis() - currentTime;
				if (diff < minDiff) {
					minDiff = diff;
				}
			}
		}

		if (minDiff != Long.MAX_VALUE) {
			formatElapsedTime(minDiff, elapsedTextView);
		} else {
			elapsedTextView.setText("");
		}
	}

	private Calendar createCalendar(Alarm alarm, Day day) {
		Calendar calendar = null;
		if (day.type == Calendar.DAY_OF_WEEK) {
			calendar = AlarmService.createDayOfWeekCalendar(alarm, day);
		} else {
			calendar = AlarmService.createDayOfYearCalendar(alarm, day);
		}
		return calendar;
	}

	private void formatElapsedTime(long different, TextView elapsedTextView) {
		long secondsInMilli = 1000;
		long minutesInMilli = secondsInMilli * 60;
		long hoursInMilli = minutesInMilli * 60;
		long daysInMilli = hoursInMilli * 24;

		long elapsedDays = different / daysInMilli;
		different = different % daysInMilli;

		long elapsedHours = different / hoursInMilli;
		different = different % hoursInMilli;

		long elapsedMinutes = different / minutesInMilli;
		different = different % minutesInMilli;

		StringBuilder builder = new StringBuilder();
		builder.append("in ");
		if (elapsedDays > 0) {
			builder.append(elapsedDays);
			builder.append(" d. ");
		}

		if (elapsedHours > 0) {
			builder.append(elapsedHours);
			builder.append(" h. ");
		}

		if (elapsedMinutes > 0) {
			builder.append(elapsedMinutes);
			builder.append(" m. ");
		}

		String text = builder.toString();

		elapsedTextView.setText(text);
	}

	private void formatDays(View view, Alarm alarm) {
		List<Day> days = new ArrayList<Day>(alarm.days);
		Collections.sort(days);
		TextView textView = (TextView) view;
		StringBuilder builder = new StringBuilder();
		for (Day day : days) {
			if (day.type == Calendar.DAY_OF_WEEK && day.selected) {
				builder.append(daysOfWeekNames[day.dayOfWeek - 1]);
				builder.append(" ");
			}
		}

		for (Day day : days) {
			if (day.type == Calendar.DAY_OF_YEAR & day.selected) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTimeInMillis(day.day);
				String text = dateFormat.format(calendar.getTime());
				builder.append("\r\n");
				builder.append(text);
				builder.append(" ");
			}
		}
		textView.setText(builder.toString());
	}

	@Override
	public void onCheck(CheckBox checkBox, boolean check) {
		Alarm object = (Alarm) checkBox.getTag(R.id.rippleView);
		object.isActive = check;
		alarmService.save(object);
		notifyDataSetChanged();
	}

	
	
	

}
