package com.iv.bestalarmapp.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.fortysevendeg.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.swipelistview.SwipeListView;
import com.fortysevendeg.swipelistview.SwipeListViewListener;
import com.gc.materialdesign.views.ButtonFloat;
import com.iv.bestalarmapp.ui.model.Alarm;
import com.iv.bestalarmapp.ui.service.AlarmService;
import com.iv.bestalarmapp.ui.service.IntentStorage;

public class MainActivity extends Activity {

	private SwipeListView listView;

	private AlarmService alarmService;

	private AlarmListAdapter adapter;

	private IntentStorage storage;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		alarmService = new AlarmService(this);
		storage = new IntentStorage(this);

		setContentView(R.layout.activity_main);
		listView = (SwipeListView) findViewById(R.id.listView);
		adapter = new AlarmListAdapter(this);
		listView.setSwipeListViewListener(new BaseSwipeListViewListener() {
			@Override
			public void onClickFrontView(int position) {
				editAlarm(position);
			}
		});
		listView.setAdapter(adapter);
		listView.setDivider(null);

	}

	@Override
	protected void onResume() {
		super.onResume();
		adapter.clear();
		adapter.addAll(alarmService.getAll());
	}

	public void onCreateAlarm(View view) {
		Intent intent = new Intent(this, EditAlarmActivity.class);
		startActivityForResult(intent, 101);
	}

	private void editAlarm(int position) {
		Alarm alarm = adapter.getItem(position);
		Intent intent = new Intent(this, EditAlarmActivity.class);
		storage.put(intent, alarm);
		startActivity(intent);
	}

	public void onDeleteAlarm(View view) {
		Integer pos = (Integer) view.getTag(R.id.buttonAlarmDelete);
		deleteAlarm(pos);
	}

	private void deleteAlarm(int position) {
		Alarm alarm = adapter.getItem(position);
		alarmService.delete(alarm.id);
		adapter.remove(alarm);
		listView.closeOpenedItems();
		adapter.notifyDataSetChanged();
	}

}
