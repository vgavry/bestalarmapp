package com.iv.bestalarmapp.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

import com.iv.bestalarmapp.ui.service.AlarmService;
import com.iv.bestalarmapp.ui.service.RandomResourceService;

public class NetworkStateReceiver extends BroadcastReceiver {
	
	private AlarmService alarmService;
	
	@Override
	public void onReceive(final Context context, final Intent intent) {
		final ConnectivityManager connMgr = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		final android.net.NetworkInfo wifi = connMgr
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

		final android.net.NetworkInfo mobile = connMgr
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

		if (wifi.isAvailable() || mobile.isAvailable()) {
			alarmService = new AlarmService(context);
			alarmService.loadAllRandomImages();
		}
	}

}
