package com.iv.bestalarmapp.ui.service;

import android.content.Context;

import com.iv.bestalarmapp.ui.model.Image;
import com.iv.bestalarmapp.ui.model.ImageUri;

public class RandomImageService extends RandomResourceService<Image,ImageUri>{

	public RandomImageService(Context context, AlarmService service) {
		super(context, service);
	}

	@Override
	protected void save(Image image) {
		alarmService.save(image);
	}

	@Override
	protected ImageUri createResourceUri(Image image) {
		return new ImageUri(image.getUri());
	}

	@Override
	public void save(ImageUri object) {
		alarmService.save(object);
	}
	
	

}
