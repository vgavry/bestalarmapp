package com.iv.bestalarmapp.ui.service;

import android.app.WallpaperManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.iv.bestalarmapp.ui.AlarmImageLoadingListener;
import com.iv.bestalarmapp.ui.R;
import com.iv.bestalarmapp.ui.R.drawable;
import com.iv.bestalarmapp.ui.model.Alarm;
import com.koushikdutta.ion.Ion;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class ImageService {

	public static class BestFitImage implements AlarmImageLoadingListener {

		@Override
		public void onProgressUpdate(String arg0, View arg1, int arg2, int arg3) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onLoadingCancelled(String arg0, View arg1) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
			System.out.println("ImageService.BestFitImage.onLoadingComplete()");
			if (arg1 instanceof ImageView) {
				ImageView view = (ImageView) arg1;
				if (view.getWidth()*0.85>arg2.getWidth()){
					view.setScaleType(ScaleType.CENTER_CROP);
				}else{
					view.setScaleType(ScaleType.FIT_START);
				}
				
			}

		}

		@Override
		public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onLoadingStarted(String arg0, View arg1) {
			// TODO Auto-generated method stub

		}

	}

	private static DisplayImageOptions options;
	
	public static void loadImage(Context context, ImageView imageView,
			String uri){
		loadImage(context, imageView, uri,new BestFitImage());
	}

	public static void loadImage(Context context, ImageView imageView,
			String uri, AlarmImageLoadingListener listener) {

		if (uri != null) {
			loadUIL(imageView, uri, listener);
		} else {
			WallpaperManager wallpaperManager = WallpaperManager
					.getInstance(context);
			Drawable wallpaperDrawable = wallpaperManager.getDrawable();
			imageView.setImageDrawable(wallpaperDrawable);
		}
	}

	private static void loadUIL(ImageView imageView, String uri,
			AlarmImageLoadingListener listener) {
		if (options == null) {
			options = createDisplayImageOptions();
		}

		if (imageView != null) {

			if (listener == null) {
				ImageLoader.getInstance().displayImage(uri, imageView, options);
			} else {
				ImageLoader.getInstance().displayImage(uri, imageView, options,
						listener, listener);
			}
		} else {
			if (listener == null) {
				ImageLoader.getInstance().loadImage(uri, options,new ImageLoadingListener() {
					
					@Override
					public void onLoadingStarted(String arg0, View arg1) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onLoadingCancelled(String arg0, View arg1) {
						// TODO Auto-generated method stub
						
					}
				});
			} else {
				ImageLoader.getInstance().loadImage(uri, options, listener);
			}
		}
	}

	private static DisplayImageOptions createDisplayImageOptions() {
		return new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true)
				.showImageOnLoading(R.drawable.loading1)
				.bitmapConfig(Bitmap.Config.RGB_565).build();
	}

	public static void loadDefaultImage(Context activity, ImageView imageView) {
		loadImage(activity, imageView, null, null);
	}

	public static void loadImage(Context context, String uri) {
		loadImage(context, null, uri, null);
		
	}

	public AlarmImageLoadingListener BestFitImage() {
		// TODO Auto-generated method stub
		return null;
	}

}
