package com.iv.bestalarmapp.ui.service;

import java.io.Serializable;

import com.iv.bestalarmapp.ui.model.Alarm;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class IntentStorage {

	private Activity activity;
	
	public IntentStorage(Activity alarmEditor) {
		this.activity = alarmEditor;
	}

	public void put(Serializable alarm) {
		Intent intent = activity.getIntent();
		put(intent, alarm);
	}

	public static void put(Intent intent, Serializable alarm) {
		intent.putExtra(alarm.getClass().getSimpleName(),alarm);
	}
	
	public <T extends Serializable> T get(Class<T> obj){
		Intent intent = activity.getIntent();
		return get(intent, obj);
	}

	public static <T extends Serializable> T get(Intent intent, Class<T> obj) {
		T alarm = null;
		Bundle extras = intent.getExtras();
		String key = obj.getSimpleName();
		if (extras == null || extras.get(key)==null) {
			alarm = newInstance(obj, alarm);
			intent.putExtra(key, (T)alarm);
		} else {
			alarm = (T)extras.get(key);
		}
		return alarm;
	}

	private static <T extends Serializable> T newInstance(Class<T> obj, T alarm) {
		try {
			alarm = obj.newInstance();
		} catch (Exception e) {
			Log.e("IntentService create instance", e.toString(),e);
		}
		return alarm;
	}

}
