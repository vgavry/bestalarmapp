package com.iv.bestalarmapp.ui.service;

import android.content.Context;

import com.iv.bestalarmapp.ui.model.Image;
import com.iv.bestalarmapp.ui.model.ImageUri;
import com.iv.bestalarmapp.ui.model.Sound;
import com.iv.bestalarmapp.ui.model.SoundUri;

public class RandomSoundService extends RandomResourceService<Sound,SoundUri>{

	public RandomSoundService(Context context, AlarmService service) {
		super(context, service);
	}

	@Override
	protected void save(Sound image) {
		alarmService.save(image);
		SoundService service = new SoundService(context,alarmService);
		service.load(image);
	}

	@Override
	protected SoundUri createResourceUri(Sound image) {
		return new SoundUri(image.getUri());
	}

	@Override
	public void save(SoundUri object) {
		alarmService.save(object);
	}
	
	

}
