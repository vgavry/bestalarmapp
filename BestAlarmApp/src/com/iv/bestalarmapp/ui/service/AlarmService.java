package com.iv.bestalarmapp.ui.service;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.iv.bestalarmapp.db.AlarmRepository;
import com.iv.bestalarmapp.ui.AlarmActivity;
import com.iv.bestalarmapp.ui.model.Alarm;
import com.iv.bestalarmapp.ui.model.Day;
import com.iv.bestalarmapp.ui.model.Image;
import com.iv.bestalarmapp.ui.model.ImageUri;
import com.iv.bestalarmapp.ui.model.Sound;
import com.iv.bestalarmapp.ui.model.SoundUri;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.DeleteBuilder;

public class AlarmService {

	private Context context;

	private AlarmRepository repository;

	private AlarmManager alarmManager;
	
	private RandomImageService randomResourceService;

	private RandomSoundService randomSoundService;

	public AlarmService(Context context) {

		this.context = context;
		repository = OpenHelperManager
				.getHelper(context, AlarmRepository.class);
		alarmManager = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		randomResourceService = new RandomImageService(context,this);
		randomSoundService = new RandomSoundService(context, this);
	}

	public List<Alarm> getAll() {
		return repository.getAlarmDao().queryForAll();
	}

	public void save(Alarm alarm) {
		cancelAlarm(alarm);
		Image image = alarm.image;
		if (image != null) {
			repository.getImageDao().createOrUpdate(image);
		}
		Sound sound = alarm.sound;
		if (sound != null) {
			repository.getSoundDao().createOrUpdate(sound);
		}
		Collection<Day> days = alarm.days;
		repository.getAlarmDao().createOrUpdate(alarm);
		deleteAlarmDays(alarm);

		for (Day day : days) {
			day.alarm = alarm;
			repository.getDayDao().createOrUpdate(day);
		}
		if (alarm.isActive) {
			schedule(alarm);
		}
		
		randomResourceService.load(alarm.image);
		randomSoundService.load(alarm.sound);
	}

	private void cancelAlarm(Alarm alarm) {
		Collection<Day> days = alarm.days;
		for (Day day : days) {
			cancelAlarm(alarm, day);
		}
	}

	private void cancelAlarm(Alarm alarm, Day day) {
		AlarmManager mgr = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		PendingIntent pi = createPendingIntent(alarm, day);
		mgr.cancel(pi);
	}

	private void schedule(Alarm alarm) {
		Collection<Day> days = alarm.days;
		for (Day day : days) {
			if (day.selected) {
				if (day.type == Calendar.DAY_OF_WEEK) {
					scheduleDayOfWeek(alarm, day);
				} else {
					scheduleDayOfYear(alarm, day);
				}
			}
		}
	}

	private void scheduleDayOfYear(Alarm alarm, Day day) {
		Calendar calendar = createDayOfYearCalendar(alarm, day);
		if (calendar!=null) {
			PendingIntent pi = createPendingIntent(alarm, day);
			alarmManager.set(AlarmManager.RTC_WAKEUP,
					calendar.getTimeInMillis(), pi);
		}
	}

	public static Calendar createDayOfYearCalendar(Alarm alarm, Day day) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(day.day);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MINUTE, alarm.minute);
		calendar.set(Calendar.HOUR_OF_DAY, alarm.hour);
		Calendar currentCalender = Calendar.getInstance();
		if (!calendar.after(currentCalender)) {
			calendar=null;
		}
		return calendar;
	}

	private void scheduleDayOfWeek(Alarm alarm, Day day) {
		Calendar calendar = createDayOfWeekCalendar(alarm, day);

		PendingIntent pi = createPendingIntent(alarm, day);

		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
				calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY * 7, pi);
	}

	public static Calendar createDayOfWeekCalendar(Alarm alarm, Day day) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MINUTE, alarm.minute);
		calendar.set(Calendar.HOUR_OF_DAY, alarm.hour);
		calendar.set(Calendar.DAY_OF_WEEK, day.dayOfWeek);
		Calendar currentCalender = Calendar.getInstance();
		if (calendar.before(currentCalender)) {
			calendar.add(Calendar.WEEK_OF_YEAR, 1);
		}
		return calendar;
	}

	private PendingIntent createPendingIntent(Alarm alarm, Day day) {
		Intent i = new Intent(context, AlarmReceiver.class);
		i.putExtra("id", alarm.id);
		i.putExtra("day_id", day.id);
		PendingIntent pi = PendingIntent.getBroadcast(context, alarm.id * 10000
				+ day.id, i, 0);
		return pi;
	}

	private RuntimeExceptionDao<Day, Integer> deleteAlarmDays(Alarm alarm) {
		RuntimeExceptionDao<Day, Integer> dayDao = repository.getDayDao();
		try {
			DeleteBuilder<Day, Integer> builder = dayDao.deleteBuilder();
			builder.where().eq("alarm_id", alarm.id);
			dayDao.delete(builder.prepare());
		} catch (SQLException e) {
			Log.e(getClass().getSimpleName(), e.toString(), e);
		}
		return dayDao;
	}

	public Alarm load(Integer id) {
		return repository.getAlarmDao().queryForId(id);
	}

	public void delete(int id) {
		repository.getAlarmDao().deleteById(id);
	}

	public void destroy() {
		if (repository != null) {
			OpenHelperManager.releaseHelper();
			repository = null;
		}
	}

	public void save(Image image) {
		repository.getImageDao().createOrUpdate(image);
	}

	public void loadAllRandomImages() {
		List<Alarm> alarms = getAll();
		for (Alarm alarm : alarms) {
			randomResourceService.loadInBackground(alarm.image);
			randomSoundService.loadInBackground(alarm.sound);
		}
		
	}
	
	public void shceduleAllAlarms() {
		List<Alarm> alarms = getAll();
		for (Alarm alarm : alarms) {
			schedule(alarm);
		}
	}

	public void save(ImageUri object) {
		repository.getImageUriDao().createOrUpdate(object);
	}

	public void save(Sound image) {
		repository.getSoundDao().createOrUpdate(image);
	}

	public void save(SoundUri object) {
		repository.getSoundUriDao().createOrUpdate(object);
	}

}
