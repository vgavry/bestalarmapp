package com.iv.bestalarmapp.ui.service;

import java.util.Collections;
import java.util.List;

import com.iv.bestalarmapp.ui.model.MediaSource;
import com.iv.bestalarmapp.ui.model.Resource;
import com.iv.bestalarmapp.ui.model.SourceType;

import android.content.Context;

public class NoImageSource implements MediaSource {

	@Override
	public SourceType getType() {
		return SourceType.IMAGE;
	}

	@Override
	public int getId() {
		return 0;
	}

	@Override
	public String getName() {
		return "System Walpaper";
	}
	
	@Override
	public List<Resource> load(Context context, String filter) {
		return Collections.emptyList();
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isOnline() {
		// TODO Auto-generated method stub
		return false;
	}

}
