package com.iv.bestalarmapp.ui.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.iv.bestalarmapp.ui.model.MediaSource;
import com.iv.bestalarmapp.ui.model.SourceType;

public class MediaSourceService {

	private List<MediaSource> sources = new ArrayList<MediaSource>();
	
	private Map<Integer, MediaSource> map = new HashMap<Integer, MediaSource>();

	public MediaSourceService() {
		super();
		add(new NoImageSource());
		add(new LocalImageSource());
		add(new FlickrImageSource());
		add(new NoSoundSource());
		add(new RingtonesSource());
		add(new LocalSoundSource());
		add(new SoundCloudSource());
		add(new GoogleImageSource());
	}

	private void add(MediaSource source) {
		sources.add(source);
		map.put(source.getId(), source);
	}

	
	public MediaSource getSource(int id) {
		return map.get(id);
	}
	
	public List<MediaSource> getSources(SourceType type) {
		List<MediaSource> list = new ArrayList<MediaSource>();
		for (MediaSource source : sources) {
			if (source.getType().equals(type)) {
				list.add(source);
			}
		}
		return list;
	}

}
