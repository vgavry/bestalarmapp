package com.iv.bestalarmapp.ui.service;

import java.io.File;
import java.util.List;

import android.database.Cursor;
import android.provider.MediaStore;

import com.iv.bestalarmapp.ui.model.Image;
import com.iv.bestalarmapp.ui.model.MediaSource;
import com.iv.bestalarmapp.ui.model.SourceType;

public class LocalImageSource extends AbstractLocalMediaSource<Image> implements MediaSource {

	

	@Override
	public SourceType getType() {
		return SourceType.IMAGE;
	}

	@Override
	public int getId() {
		return 1;
	}

	@Override
	public String getName() {
		return "Images on device";
	}

	public int getMediaType() {
		return MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
	}

	@Override
	protected void addItem(Cursor mediaCursor, List<Image> list, String filter) {
		String uri = mediaCursor.getString(mediaCursor
				.getColumnIndex(MediaStore.Files.FileColumns.DATA));
		
		if (MediaSourceUtils.notContaines(filter, uri)){
			return;
		}
		
		File file = new File(uri);
		// turn this into a file uri if necessary/possible
		if (file.exists())
			list.add(new Image("file:///"+uri));
		else
			list.add(new Image(uri));
		
	}

	@Override
	public boolean isOnline() {
		return false;
	}

	

}
