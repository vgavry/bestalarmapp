package com.iv.bestalarmapp.ui.service;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.http.conn.util.InetAddressUtils;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.iv.bestalarmapp.ui.model.Image;
import com.iv.bestalarmapp.ui.model.MediaSource;
import com.iv.bestalarmapp.ui.model.Resource;
import com.iv.bestalarmapp.ui.model.SourceType;
import com.koushikdutta.ion.Ion;

public class GoogleImageSource implements MediaSource {

	@Override
	public SourceType getType() {
		return SourceType.IMAGE;
	}

	@Override
	public int getId() {
		return 4;
	}

	@Override
	public String getName() {
		return "Google images";
	}

	@Override
	public List<Resource> load(Context context, String filter) {
		List<Resource> resources = new ArrayList<Resource>();
		try {
			if (filter == null || filter.isEmpty()) {
				filter = "quotes";
			}
			filter = URLEncoder.encode(filter, "UTF-8");
			String url = "https://www.googleapis.com/customsearch/v1?key=AIzaSyBZpxLnBDJp8NHq9H0_tRFCR6Hqb7EG16k&cx=003334065414139322029:ebdclnqteu8&searchType=image"
					+ getIpParam() + "&q=" + filter;
			JsonObject jsonObject = Ion.with(context).load(url).asJsonObject()
					.get(3, TimeUnit.MINUTES);

			JsonArray jsonArray = jsonObject.getAsJsonArray("items");
			for (int i = 0; i < jsonArray.size(); i++) {
				resources.add(new Image(jsonArray.get(i).getAsJsonObject()
						.get("link").getAsString()));
			}

		} catch (Throwable e) {
			Log.e(getClass().getName(), e.toString(), e);
		}
		return resources;
	}

	private String getIpParam() {
		return "&userIp="+getIPAddress(false);
	}

	@Override
	public void clear() {
		
	}

	@Override
	public boolean isOnline() {
		return true;
	}

	public static String getIPAddress(boolean useIPv4) {
		try {
			List<NetworkInterface> interfaces = Collections
					.list(NetworkInterface.getNetworkInterfaces());
			for (NetworkInterface intf : interfaces) {
				List<InetAddress> addrs = Collections.list(intf
						.getInetAddresses());
				for (InetAddress addr : addrs) {
					if (!addr.isLoopbackAddress()) {
						String sAddr = addr.getHostAddress().toUpperCase();
						boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
						if (useIPv4) {
							if (isIPv4)
								return sAddr;
						} else {
							if (!isIPv4) {
								int delim = sAddr.indexOf('%'); // drop ip6 port
																// suffix
								return delim < 0 ? sAddr : sAddr.substring(0,
										delim);
							}
						}
					}
				}
			}
		} catch (Exception ex) {
		} // for now eat exceptions
		return "";
	}

}
