package com.iv.bestalarmapp.ui.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import com.iv.bestalarmapp.ui.model.Vibration;

import android.content.Context;
import android.os.Vibrator;

public class VibrationService {

	private Context context;
	private AtomicBoolean isVibrationg = new AtomicBoolean(false);
	private int playingIndex = -1;

	public VibrationService(Context context) {
		super();
		this.context = context;
	}

	public List<Vibration> getVibrations() {
		List<Vibration> list = new ArrayList<Vibration>();
		add(list, "No vibration", new long[] {});
		add(list, "James Bond 007", new long[] { 0, 200, 100, 200, 275, 425,
				100, 200, 100, 200, 275, 425, 100, 75, 25, 75, 125, 75, 25, 75,
				125, 100, 100 });
		add(list, "Morse Code SOS", new long[] { 0, 100, 30, 100, 30, 100, 200,
				200, 30, 200, 30, 200, 200, 100, 30, 100, 30, 100 });
		add(list, "Voltron Theme", new long[] { 0, 250, 200, 150, 150, 100, 50,
				450, 450, 150, 150, 100, 50, 900, 2250 });

		add(list, "Darth Vader's Theme", new long[] { 0, 500, 110, 500, 110,
				450, 110, 200, 110, 170, 40, 450, 110, 200, 110, 170, 40, 500 });
		
		add(list, "Mortal Kombat Theme", new long[] { 0, 100, 200, 100, 200,
				100, 200, 100, 200, 100, 100, 100, 100, 100, 200, 100, 200,
				100, 200, 100, 200, 100, 100, 100, 100, 100, 200, 100, 200,
				100, 200, 100, 200, 100, 100, 100, 100, 100, 100, 100, 100,
				100, 100, 50, 50, 100, 800 });
		add(list, "Final Fantasy Victory Fanfare", new long[] { 0, 50, 100, 50,
				100, 50, 100, 400, 100, 300, 100, 350, 50, 200, 100, 100, 50,
				600 });

		add(list, "Super Mario Theme Intro", new long[] { 0, 125, 75, 125, 275,
				200, 275, 125, 75, 125, 275, 200, 600, 200, 600 });

		add(list, "Custom 1", new long[] { 0, 250, 200, 250, 150, 150, 75, 150,
				75, 150 });

		add(list, "Shave and a Haircut", new long[] { 0, 100, 200, 100, 100,
				100, 100, 100, 200, 100, 500, 100, 225, 100 });

		add(list, "Victory", new long[] { 0, 50, 100, 50, 100, 50, 100, 400,
				100, 300, 100, 350, 50, 200, 100, 100, 50, 600 });

		add(list, "Triangle", new long[] { 0, 200, 50, 175, 50, 150, 50, 125,
				50, 100, 50, 75, 50, 50, 50, 75, 50, 100, 50, 125, 50, 150, 50,
				157, 50, 200 });

		return list;
	}

	private void add(List<Vibration> list, String string, long[] ls) {
		list.add(new Vibration(string, ls));
	}

	public void play(int index) {
		Vibration vibration = getVibrations().get(index);
		Vibrator v = getVibrator();
		if (v != null && v.hasVibrator()) {
			if (isVibrationg.get()) {
				v.cancel();
				isVibrationg.set(false);
			}
			if (playingIndex != index && vibration.getPattern().length > 0) {
				v.vibrate(vibration.getPattern(), 0);
				playingIndex = index;
				isVibrationg.set(true);
			} else {
				playingIndex = -1;
			}
		}
	}

	public boolean isPlaying(int position) {
		return playingIndex == position && isVibrationg.get();
	}

	private Vibrator getVibrator() {
		Vibrator v = (Vibrator) context
				.getSystemService(Context.VIBRATOR_SERVICE);
		return v;
	}

	public void stop() {
		getVibrator().cancel();
	}
}
