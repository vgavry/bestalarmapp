package com.iv.bestalarmapp.ui.service;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.iv.bestalarmapp.ui.model.MediaSource;
import com.iv.bestalarmapp.ui.model.Resource;
import com.iv.bestalarmapp.ui.model.Sound;
import com.iv.bestalarmapp.ui.model.SourceType;
import com.koushikdutta.ion.Ion;

public class SoundCloudSource implements MediaSource {

	@Override
	public SourceType getType() {
		return SourceType.SOUND;
	}

	@Override
	public int getId() {
		return 105;
	}

	@Override
	public String getName() {
		return "Sound Cloud sounds";
	}

	@Override
	public List<Resource> load(Context context, String filter) {
		List<Resource> resources = new ArrayList<Resource>();
		try {
			if (filter == null || filter.isEmpty()) {
				filter = "top";
			}
			filter = URLEncoder.encode(filter, "UTF-8");
			String url = "http://bestalarmservice.appspot.com/music?k="
					+ filter;
			JsonObject jsonObject = Ion.with(context).load(url).asJsonObject()
					.get(3, TimeUnit.MINUTES);

			JsonArray jsonArray = jsonObject.getAsJsonArray("records");
			for (int i = 0; i < jsonArray.size(); i++) {
				JsonObject asJsonObject = jsonArray.get(i).getAsJsonObject();
				String stream = asJsonObject.get("stream").getAsString();
				String title = asJsonObject.get("title").getAsString();
				Sound object = new Sound(stream);
				object.onlineUri = stream;
				object.title = title;
				JsonElement iconElement = asJsonObject.get("icon");
				if (iconElement != null) {
					object.imageUri = iconElement.getAsString();
				}
				resources.add(object);
			}

		} catch (Throwable e) {
			Log.e(getClass().getName(), e.toString(), e);
		}
		return resources;
	}

	@Override
	public void clear() {

	}

	@Override
	public boolean isOnline() {
		return true;
	}

}
