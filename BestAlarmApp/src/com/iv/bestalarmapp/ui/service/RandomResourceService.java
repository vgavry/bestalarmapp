package com.iv.bestalarmapp.ui.service;

import java.util.Collection;
import java.util.List;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;

import com.iv.bestalarmapp.ui.model.MediaSource;
import com.iv.bestalarmapp.ui.model.Resource;
import com.iv.bestalarmapp.ui.model.ResourceUri;

public abstract class RandomResourceService<T extends Resource,K extends ResourceUri> {

	protected Context context;

	protected AlarmService alarmService;

	protected MediaSourceService mediaSourceService;

	private Handler handler;

	public RandomResourceService(Context context, AlarmService service) {
		super();
		this.context = context;
		alarmService = service;
		mediaSourceService = new MediaSourceService();
		handler = new Handler();

	}

	public void load(final T image) {
		AsyncTask<Void, Void, Void> asyncTask = new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				loadInBackground(image);
				return null;
			}

		};

		asyncTask.execute();

	}

	public void loadInBackground(T image) {
		if (image != null && image.isRandom() && !image.isRandomUriLoaded()) {

			MediaSource mediaSource = mediaSourceService
					.getSource(image.getMediaSourceId());

			if (mediaSource.isOnline()) {
				if (isNetworkAvailable()) {
					doLoad(image);
				}
			}else{
				doLoad(image);
			}
		}
	}

	public void reload(T image) {
		clear(image);
		load(image);
	}

	public void clear(T image) {
		if (image.isRandom()) {
			image.setRandomUriLoaded(false);
			if (image.getUri() != null) {
				saveRandomUri(image);
				save(image);
			}
		}
	}

	
	public void saveRandomUri(T image) {
		K object = createResourceUri(image);
		object.setResource(image);
		save(object);
		image.getRandomUrisHistory().add(object);
	}

	public abstract void save(K object) ;

	protected abstract K createResourceUri(T image);

	private void doLoad(T image) {
		MediaSource mediaSource = mediaSourceService
				.getSource(image.getMediaSourceId());
		List<T> list = mediaSource.load(context, image.getFilter());
		for (T resource : list) {
			Collection<ResourceUri> uris = image.getRandomUrisHistory();
			if (!uris.contains(createResourceUri(resource))) {
				image.setUri(resource.getUri());
				image.setRandomUriLoaded(true);
				save(image);
				ImageService.loadImage(context, image.getUri());
				break;
			}
		}
	}

	

	protected abstract void save(T image);

	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
}
