package com.iv.bestalarmapp.ui.service;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.iv.bestalarmapp.ui.model.Image;
import com.iv.bestalarmapp.ui.model.MediaSource;
import com.iv.bestalarmapp.ui.model.Resource;
import com.iv.bestalarmapp.ui.model.SourceType;
import com.koushikdutta.ion.Ion;

public class FlickrImageSource implements MediaSource {

	@Override
	public SourceType getType() {
		return SourceType.IMAGE;
	}

	@Override
	public int getId() {
		return 3;
	}

	@Override
	public String getName() {
		return "Flickr images";
	}

	@Override
	public List<Resource> load(Context context, String filter) {
		List<Resource> resources = new ArrayList<Resource>();
		try {
			if (filter == null || filter.isEmpty()) {
				filter = "quotes";
			}
			filter = URLEncoder.encode(filter,"UTF-8");
			String url = "http://bestalarmservice.appspot.com/image?k="+ filter;
			JsonObject jsonObject = Ion
					.with(context)
					.load(url).asJsonObject().get(3, TimeUnit.MINUTES);

			JsonArray jsonArray = jsonObject.getAsJsonArray("photos");
			for (int i = 0; i < jsonArray.size(); i++) {
				resources.add(new Image(jsonArray.get(i).getAsJsonObject()
						.get("url").getAsString()));
			}

		} catch (Throwable e) {
			Log.e(getClass().getName(), e.toString(), e);
		}
		return resources;
	}

	@Override
	public void clear() {

	}

	@Override
	public boolean isOnline() {
		return true;
	}

}
