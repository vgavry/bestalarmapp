package com.iv.bestalarmapp.ui.service;

import java.io.File;
import java.util.List;

import android.database.Cursor;
import android.provider.MediaStore;

import com.iv.bestalarmapp.ui.model.Sound;
import com.iv.bestalarmapp.ui.model.SourceType;

public class LocalSoundSource extends AbstractLocalMediaSource<Sound> {

	@Override
	public SourceType getType() {
		return SourceType.SOUND;
	}

	@Override
	public int getId() {
		return 102;
	}

	@Override
	public String getName() {
		return "Sounds on device";
	}

	@Override
	public int getMediaType() {
		return MediaStore.Files.FileColumns.MEDIA_TYPE_AUDIO;
	}

	@Override
	protected void addItem(Cursor mediaCursor, List<Sound> list, String filter) {
		String uri = mediaCursor.getString(mediaCursor
				.getColumnIndex(MediaStore.Files.FileColumns.DATA));
		String title = mediaCursor.getString(mediaCursor
				.getColumnIndex(MediaStore.Files.FileColumns.TITLE));
		
		if (MediaSourceUtils.notContaines(filter, title)){
			return;
		}
		
		File file = new File(uri);
		// turn this into a file uri if necessary/possible
		if (file.exists()) {
			Sound object = new Sound("file:///" + uri);
			object.title = title;
			list.add(object);
		}

	}

	@Override
	public boolean isOnline() {
		return false;
	}
}
