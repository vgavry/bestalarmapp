package com.iv.bestalarmapp.ui.service;


public class MediaSourceUtils  {

	public static boolean notContaines(String filter, String title) {
		return filter!=null && !filter.isEmpty() && !title.toLowerCase().contains((filter.toLowerCase()));
	}
}
