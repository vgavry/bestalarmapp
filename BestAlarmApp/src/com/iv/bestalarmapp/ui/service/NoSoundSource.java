package com.iv.bestalarmapp.ui.service;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import android.content.Context;

import com.iv.bestalarmapp.ui.model.MediaSource;
import com.iv.bestalarmapp.ui.model.Resource;
import com.iv.bestalarmapp.ui.model.SourceType;

public class NoSoundSource implements MediaSource {

	@Override
	public SourceType getType() {
		return SourceType.SOUND;
	}

	@Override
	public int getId() {
		// TODO Auto-generated method stub
		return 100;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "System alarm sound";
	}

	@Override
	public List<Resource> load(Context context, String filter) {
		// TODO Auto-generated method stub
		return Collections.emptyList();
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isOnline() {
		// TODO Auto-generated method stub
		return false;
	}

}
