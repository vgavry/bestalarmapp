package com.iv.bestalarmapp.ui.service;

import java.io.File;
import java.util.UUID;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.iv.bestalarmapp.ui.model.Sound;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;

public class SoundService {

	private final class MediaPlayerTask extends AsyncTask<Void, Void, Void> {
		private final Sound sound;

		private MediaPlayerTask(Sound sound) {
			this.sound = sound;
		}

		@Override
		protected Void doInBackground(Void... params) {
			start(sound);
			return null;
		}

		public Sound getSound() {
			return sound;
		}

	}

	private Context context;
	// private Ringtone ringtone;
	private Sound sound;
	private MediaPlayer mediaPlayer;
	private AlarmService alarmService;
	private MediaPlayerTask task;

	// private OnMediaStateChangeisterer

	public SoundService(Context context, AlarmService service) {
		this.context = context;
		this.sound = new Sound();
		this.sound.isRingtone = true;
		this.sound.uri = RingtoneManager.getDefaultUri(
				RingtoneManager.TYPE_ALARM).toString();
		this.alarmService = service;
	}

	public void play(Sound sound) {
		if (sound != null) {
			doPlay(sound);
			this.sound = sound;
		} else {
			doPlay(this.sound);
		}
	}

	public void doPlay(final Sound sound) {
		stop();

		task = new MediaPlayerTask(sound);

		task.execute();

	}

	private void start(Sound sound) {
		mediaPlayer = new MediaPlayer();

		if (sound.isRingtone) {
			mediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
		} else {
			mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		}

		doPlayMedia(sound);
	}

	private void doPlayMedia(Sound sound) {
		try {
			Log.i(getClass().getName(), "Starting playing :" + sound.uri);
			mediaPlayer.setLooping(true);
			mediaPlayer.setDataSource(context, Uri.parse(sound.uri));
			mediaPlayer.prepare();
			Log.i(getClass().getName(), "Preapering playing :" + sound.uri);
			mediaPlayer.start();
			Log.i(getClass().getName(), "Playing :" + sound.uri);
		} catch (Throwable e) {
			Log.e(getClass().getSimpleName(), e.toString(), e);
		}
	}

	public void stop() {
		// if (ringtone != null) {
		// ringtone.stop();
		// ringtone = null;
		// }

		if (task != null) {
			task.cancel(true);
		}

		if (mediaPlayer != null) {
			mediaPlayer.stop();
			mediaPlayer.release();
			mediaPlayer = null;
		}

	}

	public boolean isPlaying(Sound tag) {
		return task != null && !task.isCancelled() && tag.equals(task.sound);
	}

	public boolean isPlaying() {
		return task != null && !task.isCancelled();
	}

	public void load(final Sound sound) {
		if (sound.onlineUri != null && sound.uri==sound.uri) {

			String uniqueID = UUID.randomUUID().toString();
			final File file = new File(getMusicDirectory(), uniqueID);
			Ion.with(context).load(sound.onlineUri)
					.progress(new ProgressCallback() {

						@Override
						public void onProgress(long arg0, long arg1) {
							sound.uri = file.toURI().toString();
						}
					}).write(file);
		}
	}

	public File getMusicDirectory() {
		return Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC);
	}
}
