package com.iv.bestalarmapp.ui.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.iv.bestalarmapp.ui.AlarmActivity;
import com.iv.bestalarmapp.ui.model.Alarm;

public class AlarmReceiver extends BroadcastReceiver {

	
	@Override
	public void onReceive(Context ctxt, Intent intent) {
		if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
			Log.d(getClass().getSimpleName(), "BOOT_COMPLETED");
			AlarmService alarmService = new AlarmService(ctxt);
			alarmService.shceduleAllAlarms();
		} else {
			startAlarmActivity(ctxt, intent);
		}
	}

	public void startAlarmActivity(Context ctxt, Intent intent) {
		int id = intent.getIntExtra("id", -1);
		if (id != -1) {
			Intent newIntent = new Intent(ctxt, AlarmActivity.class);
			AlarmService alarmService = new AlarmService(ctxt);
			Alarm alarm = alarmService.load(id);

			IntentStorage.put(newIntent, alarm);
			newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			newIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
			newIntent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
			ctxt.startActivity(newIntent);
		}
	}

}
