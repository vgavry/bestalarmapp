package com.iv.bestalarmapp.ui.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;

import com.iv.bestalarmapp.ui.model.Image;
import com.iv.bestalarmapp.ui.model.MediaSource;
import com.iv.bestalarmapp.ui.model.Resource;

public abstract class AbstractLocalMediaSource<T extends Resource> implements MediaSource {
	
	Cursor mediaCursor;

	public AbstractLocalMediaSource() {
		super();
	}

	@Override
	public List<T> load(Context context, String filter) {
		return loadMore(context,filter);
	}

	public List<T> loadMore(Context context, String filter) {
	
		List<T> list = new ArrayList<T>();
	
		if (mediaCursor == null) {
			mediaCursor = context.getContentResolver().query(
					MediaStore.Files.getContentUri("external"), null, null,
					null, null);
		}
	
		int loaded = 0;
		while (mediaCursor.moveToNext() && loaded < 100) {
			// get the media type. ion can show images for both regular images
			// AND video.
			int mediaType = mediaCursor.getInt(mediaCursor
					.getColumnIndex(MediaStore.Files.FileColumns.MEDIA_TYPE));
			if (mediaType != getMediaType()) {
				continue;
			}
	
			loaded++;
	
			addItem(mediaCursor,list, filter);
		}
	
		return list;
	}
	
	protected abstract void addItem(Cursor mediaCursor2, List<T> list, String filter);

	public abstract int getMediaType();
	

	@Override
	public void clear() {
		mediaCursor = null;
		
	}

}