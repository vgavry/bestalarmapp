package com.iv.bestalarmapp.ui.service;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;

import com.iv.bestalarmapp.ui.model.MediaSource;
import com.iv.bestalarmapp.ui.model.Resource;
import com.iv.bestalarmapp.ui.model.Sound;
import com.iv.bestalarmapp.ui.model.SourceType;

public class RingtonesSource implements MediaSource {

	@Override
	public SourceType getType() {
		return SourceType.SOUND;
	}

	@Override
	public int getId() {
		return 101;
	}

	@Override
	public String getName() {
		return "Ringtones";
	}

	@Override
	public List<Resource> load(Context context, String filter) {
		List<Resource> list = new ArrayList<Resource>();
		RingtoneManager manager = new RingtoneManager(context);
		manager.setType(RingtoneManager.TYPE_ALL);
		Cursor cursor = manager.getCursor();
		while (cursor.moveToNext()) {
			String title = cursor.getString(RingtoneManager.TITLE_COLUMN_INDEX);
			String uri = cursor.getString(RingtoneManager.URI_COLUMN_INDEX);
			String id = cursor.getString(RingtoneManager.ID_COLUMN_INDEX);
			
			if (MediaSourceUtils.notContaines(filter, title)){
				continue;
			}
			
			Sound sound = new Sound();
			sound.title = title;
			Uri parsedUri = Uri.parse(uri);
			sound.uri = Uri.withAppendedPath(parsedUri,id).toString();
			sound.isRingtone = true;
			list.add(sound);
		}
		return list;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isOnline() {
		// TODO Auto-generated method stub
		return false;
	}

}
