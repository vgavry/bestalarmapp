package com.iv.bestalarmapp.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.android.datetimepicker.date.DatePickerDialog;
import com.android.datetimepicker.date.DatePickerDialog.OnDateSetListener;
import com.gc.materialdesign.views.ButtonFloat;
import com.iv.bestalarmapp.ui.model.Alarm;
import com.iv.bestalarmapp.ui.model.Day;
import com.iv.bestalarmapp.ui.service.IntentStorage;

public class ChooseDaysFragment extends Fragment implements
		OnItemClickListener, OnClickListener, OnDateSetListener {

	private ListView listView;
	private ListAdapter<Day> adapter;
	private IntentStorage storage;

	public ChooseDaysFragment() {
		super();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		storage = new IntentStorage(getActivity());
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onResume() {
		super.onResume();
		adapter.clear();

		Alarm alarm = storage.get(Alarm.class);
		Collection<Day> days = alarm.days;
		if (days.isEmpty()) {
			alarm.days = addDaysOfWeek();
			storage.put(alarm);
			adapter.addAll(alarm.days);
		} else {
			for (Day day : days) {
				adapter.add(day);
			}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_days, container, false);
		listView = (ListView) v.findViewById(R.id.listView);
		adapter = new DaysListAdapter(getActivity(),this, R.layout.item_day_choice,
				R.id.text1, R.id.checkBox);
		listView.setItemsCanFocus(false);
		listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		listView.setAdapter(adapter);
		listView.setDividerHeight(0);
		listView.setOnItemClickListener(this);

		ButtonFloat buttonFlat = (ButtonFloat) v
				.findViewById(R.id.buttonNewDayAlarm);
		buttonFlat.setOnClickListener(this);
		return v;

	}

	private List<Day> addDaysOfWeek() {
		List<Day> days = new ArrayList<Day>();
		String[] stringArray = getResources().getStringArray(R.array.days);
		for (int i = 0; i < stringArray.length; i++) {
			Day day = new Day();
			day.type = Calendar.DAY_OF_WEEK;
			day.dayOfWeek = i+1;
			days.add(day);
		}

		return days;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		System.out.println("ChooseDaysFragment.onItemClick()");
		saveElement(arg2);
	}

	private void saveElement(int index) {
		Day day = adapter.getItem(index);
		saveElement(day);
	}

	public void saveElement(Day day) {
		Alarm alarm = storage.get(Alarm.class);
		if (!adapter.getItems().contains(day)){
			adapter.getItems().add(day);
		}
		day.selected = !day.selected;
		alarm.days = adapter.getItems();
		storage.put(alarm);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void onClick(View arg0) {
		DatePickerDialog dialog = new DatePickerDialog();
		dialog.show(getActivity().getFragmentManager(), "chooseDate");
		dialog.setOnDateSetListener(this);
	}

	@Override
	public void onDateSet(DatePickerDialog dialog, int year, int monthOfYear,
			int dayOfMonth) {
		// adapter.add(dayOfMonth + "." + monthOfYear + "." + year);
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, monthOfYear, dayOfMonth);
		Day day = new Day();
		day.type = Calendar.DAY_OF_YEAR;
		day.day = calendar.getTimeInMillis();
		adapter.add(day);
		saveElement(day);
	}

}
