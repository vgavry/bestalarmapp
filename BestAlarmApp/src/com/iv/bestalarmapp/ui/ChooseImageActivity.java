package com.iv.bestalarmapp.ui;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.alexvasilkov.foldablelayout.UnfoldableView;
import com.alexvasilkov.foldablelayout.shading.GlanceFoldShading;
import com.iv.bestalarmapp.ui.model.Alarm;
import com.iv.bestalarmapp.ui.model.Image;
import com.iv.bestalarmapp.ui.model.MediaSource;
import com.iv.bestalarmapp.ui.model.Resource;
import com.iv.bestalarmapp.ui.model.SourceType;
import com.iv.bestalarmapp.ui.service.ImageService;

public class ChooseImageActivity extends AbstractResourceActivity<Image> implements
		Foldable {

	private final class OnImageListener implements OnClickListener {
		
		private ScaleType scaleType = ScaleType.CENTER_CROP;
		
		@Override
		public void onClick(View v) {
			if (scaleType.equals(ScaleType.CENTER_CROP)){
				scaleType = ScaleType.FIT_CENTER;
			}else{
				scaleType = ScaleType.CENTER_CROP;
			}
			ImageView view = (ImageView) v;
			view.setScaleType(scaleType);
		}

		public void clear() {
			scaleType = ScaleType.CENTER_CROP;
		}
	}

	private UnfoldableView mUnfoldableView;
	private View mListTouchInterceptor;
	private View mDetailsLayout;
	private OnImageListener onImageListener;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mListTouchInterceptor = findViewById(R.id.touch_interceptor_view);
		mListTouchInterceptor.setClickable(false);

		mDetailsLayout = findViewById(R.id.details_layout);
		mDetailsLayout.setVisibility(View.INVISIBLE);

		mUnfoldableView = (UnfoldableView) findViewById(R.id.unfoldable_view);

		Bitmap glance = BitmapFactory.decodeResource(getResources(),
				R.drawable.unfold_glance);
		mUnfoldableView.setFoldShading(new GlanceFoldShading(this, glance));
		mUnfoldableView
				.setOnFoldingListener(new UnfoldableView.SimpleFoldingListener() {
					@Override
					public void onUnfolding(UnfoldableView unfoldableView) {
						mListTouchInterceptor.setClickable(true);
						mDetailsLayout.setVisibility(View.VISIBLE);
					}

					@Override
					public void onUnfolded(UnfoldableView unfoldableView) {
						mListTouchInterceptor.setClickable(false);
					}

					@Override
					public void onFoldingBack(UnfoldableView unfoldableView) {
						mListTouchInterceptor.setClickable(true);
					}

					@Override
					public void onFoldedBack(UnfoldableView unfoldableView) {
						mListTouchInterceptor.setClickable(false);
						mDetailsLayout.setVisibility(View.INVISIBLE);
					}
				});
	}

	public SourceType getSourceType() {
		return SourceType.IMAGE;
	}

	public ImageListAdapter createResourceAdapter() {
		return new ImageListAdapter(this);
	}

	public void createResource(Alarm alarm) {
		Image image;
		image = new Image();
		image.mediaSourceId = getMediaSources().get(0).getId();
		alarm.image = image;
	}

	@Override
	protected void mediaSourceUpdated(MediaSource currentMediaSource) {

	}

	@Override
	Image getResource(Alarm alarm) {
		return alarm.image;
	}

	@Override
	public void openView(View v, String uri) {
		ImageView image = (ImageView) mDetailsLayout
				.findViewById(R.id.details_image);
		image.setScaleType(ScaleType.CENTER_CROP);
		if (onImageListener==null){
			onImageListener = new OnImageListener();
			//image.setOnClickListener(onImageListener);
		}
		onImageListener.clear();
		ImageService.loadImage(this, image, uri);
		mUnfoldableView.unfold(v, mDetailsLayout);
	}

	@Override
	public void onBackPressed() {
		if (mUnfoldableView != null
				&& (mUnfoldableView.isUnfolded() || mUnfoldableView
						.isUnfolding())) {
			mUnfoldableView.foldBack();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	void onSaveCustomFields(Image selectedResource, Image resource2) {
		// TODO Auto-generated method stub
		
	}

}
