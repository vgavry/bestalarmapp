package com.iv.bestalarmapp.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gc.materialdesign.views.ButtonFloat;
import com.iv.bestalarmapp.ui.model.Alarm;
import com.iv.bestalarmapp.ui.model.Resource;
import com.iv.bestalarmapp.ui.model.SourceType;
import com.iv.bestalarmapp.ui.service.ImageService;
import com.iv.bestalarmapp.ui.service.ImageService.BestFitImage;
import com.iv.bestalarmapp.ui.service.IntentStorage;
import com.iv.bestalarmapp.ui.service.MediaSourceService;

public abstract class AbstractChooseResourceFragment<T extends Resource>
		extends Fragment implements OnClickListener {

	private ImageView imageView;
	private IntentStorage activityStorage;
	private MediaSourceService mediaSourceService;
	private TextView textSource;

	public AbstractChooseResourceFragment() {
		super();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		activityStorage = new IntentStorage(getActivity());
		mediaSourceService = new MediaSourceService();
	}

	@Override
	public void onResume() {
		super.onResume();
		Alarm alarm = getAlarm();
		T resource = getResource(alarm);
		if (resource != null) {
			String imageUri = getImageUri(alarm);
			loadImage(imageUri);
			String mediaSourceName = mediaSourceService.getSource(
					resource.getMediaSourceId()).getName();
			textSource.setText(getFormattedResourceInfo(mediaSourceName,
					resource));

		} else {
			textSource.setText(mediaSourceService.getSources(getSourceType())
					.get(0).getName());
		}
	}

	protected abstract String getFormattedResourceInfo(String mediaSourceName, T resource);

	public Alarm getAlarm() {
		return activityStorage.get(Alarm.class);
	}

	protected abstract String getImageUri(Alarm alarm);

	protected abstract SourceType getSourceType();

	protected abstract T getResource(Alarm alarm);

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(getLayoutResourceId(), container, false);
		ButtonFloat buttonFloat = (ButtonFloat) v
				.findViewById(R.id.buttonFloat);
		imageView = (ImageView) v.findViewById(R.id.image);
		textSource = (TextView) v.findViewById(R.id.imageInfoContainer);
		ImageService.loadDefaultImage(getActivity(), imageView);
		buttonFloat.setOnClickListener(this);
		return v;
	}

	protected abstract int getLayoutResourceId();

	@Override
	public void onClick(View arg0) {
		Intent intent = new Intent(getActivity(), getEditResourceActivity());
		activityStorage.put(intent, getAlarm());
		startActivityForResult(intent, RequestCodes.CHOOSE_IMAGE);
	}

	protected abstract Class<?> getEditResourceActivity();

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			Alarm alarm = activityStorage.get(data, Alarm.class);
			activityStorage.put(alarm);
			if (getResource(alarm) != null) {
				String uri = getResource(alarm).getUri();
				loadImage(uri);
			}
		}
	}

	private void loadImage(String uri) {
		ImageService.loadImage(getActivity(), imageView, uri);
	}

}