package com.iv.bestalarmapp.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.iv.bestalarmapp.ui.model.Alarm;
import com.iv.bestalarmapp.ui.model.Sound;
import com.iv.bestalarmapp.ui.model.SourceType;
import com.iv.bestalarmapp.ui.service.AlarmService;
import com.iv.bestalarmapp.ui.service.SoundService;

public class ChooseSoundFragment extends AbstractChooseResourceFragment<Sound> {

	private ImageView imageView;

	private SoundService soundService;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		soundService = new SoundService(getActivity(),new AlarmService(getActivity()));
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		imageView = (ImageView) view.findViewById(R.id.buttonSoundPlay);
		imageView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Sound sound = getAlarm().sound;
				if (soundService.isPlaying()){
					soundService.stop();
					imageView.setImageResource(R.drawable.ic_play_circle_outline_black_48dp);
				}else{
					imageView.setImageResource(R.drawable.ic_pause_circle_outline_black_48dp);
					soundService.play(sound);
				}
			}
		});
		return view;
	}

	public ChooseSoundFragment() {
		super();
	}

	protected Class<?> getEditResourceActivity() {
		return ChooseSoundActivity.class;
	}

	protected Sound getResource(Alarm alarm) {
		return alarm.sound;
	}

	protected int getLayoutResourceId() {
		return R.layout.fragment_sound;
	}

	@Override
	protected SourceType getSourceType() {
		return SourceType.SOUND;
	}

	@Override
	protected String getImageUri(Alarm alarm) {
		return alarm.sound.imageUri;
	}

	@Override
	protected String getFormattedResourceInfo(String mediaSourceName,
			Sound resource) {
		StringBuilder builder = new StringBuilder();
		if (resource.isRandom){
			builder.append("Random image");
		}else{
			builder.append("Sound ");
			builder.append("'");
			builder.append(resource.title);
			builder.append("'");
		}
		
		builder.append("\r\nfrom '");
		builder.append(mediaSourceName);
		builder.append("'");
		
		if (resource.filter!=null && !resource.filter.isEmpty()){
			builder.append("\r\nby filter '");
			builder.append(resource.filter);
			builder.append("'");
		}
		return builder.toString();
	}
}
