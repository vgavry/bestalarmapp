package com.iv.bestalarmapp.ui;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.gc.materialdesign.views.CheckBox;
import com.iv.bestalarmapp.ui.model.Sound;
import com.iv.bestalarmapp.ui.service.AlarmService;
import com.iv.bestalarmapp.ui.service.SoundService;

public class SoundListAdapter extends ListAdapter<Sound> implements
		OnClickListener {

	private SoundService service;

	public SoundListAdapter(Activity context) {
		super(context, R.layout.item_sound, R.id.text1, R.id.imageButton,
				R.id.rippleView, R.id.checkBox);
		service = new SoundService(context,new AlarmService(context));
	}

	@Override
	protected void formatElement(Integer integer, View view, Sound object,
			View parenView, int position, boolean checked) {

		switch (integer) {
		case R.id.text1:
			TextView textView = (TextView) view;
			textView.setText(object.title);
			break;

		case R.id.imageButton:
			ImageButton imageView = (ImageButton) view;
			imageView.setTag(R.id.imageButton,object);
			imageView.setOnClickListener(this);
			boolean isPlaying = service.isPlaying(object);
			if (isPlaying){
				imageView.setImageResource(R.drawable.ic_pause_circle_outline_black_36dp);
			}else{
				imageView.setImageResource(R.drawable.ic_play_circle_outline_black_36dp);
			}
			break;
		case R.id.rippleView:
			view.setTag(R.id.rippleView, position);
			view.setOnClickListener(this);
			break;

		case R.id.checkBox:
			CheckBox box = (CheckBox) view;
			box.setChecked(checked);
			break;

		default:
			break;
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.rippleView:
			Integer position = (Integer) v.getTag(R.id.rippleView);
			setSelectedItemPosition(position);
			break;
		case R.id.imageButton:
			commandSound((Sound) v.getTag(R.id.imageButton));
			break;
		default:
			break;
		}

	}

	private void commandSound(Sound tag) {
		if (service.isPlaying(tag)) {
			service.stop();
		} else {
			service.play(tag);
		}
		notifyDataSetChanged();
	}

}
