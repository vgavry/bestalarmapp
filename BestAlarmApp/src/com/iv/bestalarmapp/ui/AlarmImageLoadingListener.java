package com.iv.bestalarmapp.ui;

import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;

public interface AlarmImageLoadingListener extends ImageLoadingProgressListener, ImageLoadingListener{

}
