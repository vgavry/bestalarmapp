package com.iv.bestalarmapp.ui;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.gc.materialdesign.views.ButtonFlat;
import com.gc.materialdesign.views.CheckBox;
import com.gc.materialdesign.views.ProgressBarCircularIndetermininate;
import com.iv.bestalarmapp.ui.model.Alarm;
import com.iv.bestalarmapp.ui.model.MediaSource;
import com.iv.bestalarmapp.ui.model.Resource;
import com.iv.bestalarmapp.ui.model.SourceType;
import com.iv.bestalarmapp.ui.service.IntentStorage;
import com.iv.bestalarmapp.ui.service.MediaSourceService;
import com.koushikdutta.ion.Ion;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.u1aryz.android.lib.newpopupmenu.MenuItem;
import com.u1aryz.android.lib.newpopupmenu.PopupMenu;
import com.u1aryz.android.lib.newpopupmenu.PopupMenu.OnItemSelectedListener;

public abstract class AbstractResourceActivity<T extends Resource> extends Activity implements
		LoaderManager.LoaderCallbacks<List<Resource>>, OnItemClickListener,
		OnItemSelectedListener, OnClickListener, OnEditorActionListener {

	private ListView listView;
	protected ListAdapter<Resource> adapter;
	protected MediaSourceService mediaSourceService;
	private ProgressBarCircularIndetermininate progress;
	private MediaSource currentMediaSource;
	private Button source;
	protected IntentStorage storage;
	private AutoCompleteTextView filterView;
	private CheckBox randomCheckBox;

	public AbstractResourceActivity() {
		super();
	}

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		mediaSourceService = new MediaSourceService();
		storage = new IntentStorage(this);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_choose_resource);

		randomCheckBox = (CheckBox) findViewById(R.id.checkBox);

		listView = (ListView) findViewById(R.id.listView);
		adapter = createResourceAdapter();
		listView.setAdapter(adapter);
		listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		listView.setDivider(null);
		listView.setOnItemClickListener(this);

		progress = (ProgressBarCircularIndetermininate) findViewById(R.id.progress);
		source = (Button) findViewById(R.id.dropDown);

		ButtonFlat buttonFlat = (ButtonFlat) findViewById(R.id.imageButtonAccept);
		buttonFlat.setOnClickListener(this);

		filterView = (AutoCompleteTextView) findViewById(R.id.filter);
		filterView.setOnEditorActionListener(this);

		// updateSource();
	}

	protected MediaSource loadMediaSource() {
		Resource image = getResource(getAlarm());
		return mediaSourceService.getSource(image.getMediaSourceId());
	}

	protected List<MediaSource> getMediaSources() {
		return mediaSourceService.getSources(getSourceType());
	}

	public abstract ListAdapter createResourceAdapter();

	public abstract SourceType getSourceType();

	private void updateSource() {
		source.setText(currentMediaSource.getName());
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		reloadSourceData();
	}

	private void reloadSourceData() {
		// Ion.getBitmapLoadExecutorService().shutdown();
		if (currentMediaSource == null) {
			currentMediaSource = loadMediaSource();
			Resource resource = getResource(getAlarm());
			filterView.setText(resource.getFilter());
			randomCheckBox.setChecked(resource.isRandom());
		}
		
		updateSource();
		adapter.clear();
		Ion.getDefault(this).cancelAll();
		ImageLoader.getInstance().stop();
		currentMediaSource.clear();
		progress.restart();
		progress.setVisibility(View.VISIBLE);
		getLoaderManager().restartLoader(0, null, this);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		adapter.setSelectedItemPosition(arg2);
	}

	@Override
	public Loader<List<Resource>> onCreateLoader(int arg0, Bundle arg1) {
		return new AsyncTaskLoader<List<Resource>>(this) {

			@Override
			public List<Resource> loadInBackground() {
				String filter = filterView.getText().toString();
				return currentMediaSource.load(AbstractResourceActivity.this,
						filter);
			}

			@Override
			protected void onStartLoading() {
				super.onStartLoading();
				forceLoad();
			}

			@Override
			public void stopLoading() {
				super.stopLoading();

			}
		};
	}

	@Override
	public void onLoadFinished(Loader<List<Resource>> arg0,
			List<Resource> results) {
		Resource currentResource = getResource(getAlarm());
		adapter.clear();
		adapter.addAll(results);
		int selectedPosition = -1;
		for (int i = 0; i < results.size(); i++) {
			Resource resource = results.get(i);
			if (!currentResource.isRandom()
					&& resource.getUri().equals(currentResource.getUri())) {
				adapter.setSelectedItemPosition(i);
				selectedPosition = i;
				break;
			}
		}
		progress.setVisibility(View.GONE);
		if (selectedPosition != -1) {
			listView.smoothScrollToPosition(selectedPosition);
		}
	}

	@Override
	public void onLoaderReset(Loader<List<Resource>> arg0) {
		adapter.clear();
	}

	public void onChangeSource(View view) {
		List<MediaSource> sources = getMediaSources();
		PopupMenu popup = new PopupMenu(this);
		popup.setOnItemSelectedListener(this);
		for (MediaSource source : sources) {
			popup.add(source.getId(), source.getName());
		}
		popup.show(view);
	}

	@Override
	public void onItemSelected(MenuItem item) {
		MediaSource selected = mediaSourceService.getSource(item.getItemId());
		if (selected.getId() != currentMediaSource.getId()) {
			currentMediaSource = selected;
			reloadSourceData();
			updateSource();
			mediaSourceUpdated(currentMediaSource);
		}
	}

	protected abstract void mediaSourceUpdated(MediaSource currentMediaSource2);

	@Override
	public void onClick(View v) {
		String filter = filterView.getText().toString();
		Boolean random = randomCheckBox.isCheck();
		T resource = (T) adapter.getSelectedItem();
		onSave(currentMediaSource, filter, random, resource);

		Intent intent = new Intent();
		storage.put(intent, storage.get(Alarm.class));

		setResult(RESULT_OK, intent);
		finish();
	}

	protected void onSave(MediaSource currentMediaSource, String filter,
			Boolean random, T selectedResource) {
		Alarm alarm = getAlarm();
		T resource2 = getResource(alarm);
		resource2.setFilter(filter);
		resource2.setRandom(random);
		resource2.setMediaSourceId(currentMediaSource.getId());
		if (selectedResource != null) {
			onSaveCustomFields(selectedResource,resource2);
			resource2.setUri(selectedResource.getUri());
		}
		storage.put(alarm);
	}

	abstract void onSaveCustomFields(T selectedResource, T resource2);
	
	abstract T getResource(Alarm alarm);

	public Alarm getAlarm() {
		Alarm alarm = storage.get(Alarm.class);
		Resource image = getResource(alarm);
		if (image == null) {
			createResource(alarm);
			storage.put(alarm);
		}
		return alarm;
	}

	abstract void createResource(Alarm alarm);

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if (actionId == EditorInfo.IME_ACTION_SEARCH) {
			filterView.clearFocus();
			InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			in.hideSoftInputFromWindow(filterView.getWindowToken(), 0);
			reloadSourceData();
			return true;
		}
		return false;
	}

}