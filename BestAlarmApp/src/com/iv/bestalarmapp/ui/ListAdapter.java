package com.iv.bestalarmapp.ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.iv.bestalarmapp.ui.model.Day;
import com.iv.bestalarmapp.ui.model.Resource;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public abstract class ListAdapter<T> extends BaseAdapter {

	private HashMap<Integer, Boolean> myChecked = new HashMap<Integer, Boolean>();
	private LayoutInflater mInflater;
	protected Context context;
	private List<T> objects = new ArrayList<T>();
	private int[] itemResourcesIds;
	private int layoutItemResourceId;
	protected Integer checkedPosition;

	public ListAdapter(Context context, int layoutItemResourceId,
			int... itemResourcesIds) {
		super();
		this.context = context;
		this.layoutItemResourceId = layoutItemResourceId;
		this.itemResourcesIds = itemResourcesIds;
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	/**
	 * The number of items in the currentMeals is determined by the number of
	 * speeches in our array.
	 * 
	 * @see android.widget.ListAdapter#getCount()
	 */
	public int getCount() {
		return objects.size();
	}

	/**
	 * Since the data comes from an array, just returning the index is sufficent
	 * to get at the data. If we were using a more complex data structure, we
	 * would return whatever object represents one row in the currentMeals.
	 * 
	 * @see android.widget.ListAdapter#getItem(int)
	 */
	public T getItem(int position) {
		return objects.get(position);
	}

	/**
	 * Use the array index as a unique id.
	 * 
	 * @see android.widget.ListAdapter#getItemId(int)
	 */
	public long getItemId(int position) {
		return position;
	}

	/**
	 * Make a view to hold each row.
	 * 
	 * @see android.widget.ListAdapter#getView(int, android.view.View,
	 *      android.view.ViewGroup)
	 */
	public View getView(int position, View convertView, ViewGroup parent) {

		final HashMap<Integer, View> holder;

		// by ListView is null.
		if (convertView == null) {
			try {
			convertView = mInflater.inflate(layoutItemResourceId, null);
			} catch (Throwable throwable){
				Log.e(getClass().getSimpleName(), throwable.toString(),throwable);
			}
			// Creates a ViewHolder and store references to the two children
			// views
			// we want to bind data to.
			holder = new HashMap<Integer, View>();
			for (int id : itemResourcesIds) {
				holder.put(id, convertView.findViewById(id));
			}
			convertView.setTag(holder);
			viewInitialized(convertView,position);
		} else {
			// Get the ViewHolder back to get fast access to the TextView
			// and the ImageView.
			holder = (HashMap<Integer, View>) convertView.getTag();
			viewUpdated(convertView,position);
		}

		T object = objects.get(position);
		formatItem(holder, object, convertView, position);

		return convertView;

	}

	protected void viewUpdated(View convertView, int position) {
		// TODO Auto-generated method stub
		
	}

	protected void viewInitialized(View convertView, int position) {
		
	}

	protected void formatItem(HashMap<Integer, View> viewMap, T object,
			View convertView, int position) {
		Boolean checked = null;
		if (checkedPosition == null) {
			checked = myChecked.get(position);
		} else {
			checked = checkedPosition == position;
		}
		if (checked == null) {
			checked = false;
		}
		Set<Integer> integers = viewMap.keySet();
		for (Integer integer : integers) {
			formatElement(integer, viewMap.get(integer), object, convertView,
					position, checked);
		}

		if (integers.isEmpty()) {
			formatElement(-1, convertView, object, convertView, position,
					checked);
		}

	}

	protected abstract void formatElement(Integer integer, View view, T object,
			View parenView, int position, boolean checked);

	public void add(T s) {
		objects.add(s);
		notifyDataSetChanged();
	}

	public void addAll(Collection<T> s) {
		objects.addAll(s);
		notifyDataSetChanged();
	}

	public void clear() {
		checkedPosition=null;
		objects.clear();
		notifyDataSetChanged();
	}

	public void addAll(int location, Collection<? extends T> collection) {
		objects.addAll(location, collection);
		notifyDataSetChanged();
	}

	public int indexOf(T object) {
		return objects.indexOf(object);
	}

	public void removeAll(Collection<?> collection) {
		objects.removeAll(collection);
		notifyDataSetChanged();
	}

	public boolean remove(Object object) {
		boolean remove = objects.remove(object);
		return remove;
	}

	public boolean toggleChecked(int position) {
		Boolean boolean1 = myChecked.get(position);
		if (boolean1 == null) {
			boolean1 = false;
		}
		if (boolean1) {
			myChecked.put(position, false);
		} else {
			myChecked.put(position, true);
		}

		notifyDataSetChanged();
		return !boolean1;
	}

	public List<Integer> getCheckedItemPositions() {
		List<Integer> checkedItemPositions = new ArrayList<Integer>();

		for (int i = 0; i < myChecked.size(); i++) {
			if (myChecked.get(i)) {
				(checkedItemPositions).add(i);
			}
		}

		return checkedItemPositions;
	}

	public List<T> getCheckedItems() {
		List<T> checkedItems = new ArrayList<T>();

		for (int i = 0; i < myChecked.size(); i++) {
			if (myChecked.get(i)) {
				(checkedItems).add(objects.get(i));
			}
		}

		return checkedItems;
	}

	public void setSelectedItemPosition(int checked) {
		this.checkedPosition = checked;
		notifyDataSetChanged();
	}

	public Integer getSelectedItemPosition() {
		return checkedPosition;
	}

	public Collection<T> getItems() {
		return objects;
	}

	public T getSelectedItem() {
		T resource = null;
		if (checkedPosition != null) {
			resource = getItem(checkedPosition);
		}
		return resource;
	}

}
