package com.iv.bestalarmapp.ui;

import android.os.Bundle;

import com.iv.bestalarmapp.ui.model.Alarm;
import com.iv.bestalarmapp.ui.model.MediaSource;
import com.iv.bestalarmapp.ui.model.Resource;
import com.iv.bestalarmapp.ui.model.Sound;
import com.iv.bestalarmapp.ui.model.SourceType;
import com.iv.bestalarmapp.ui.service.AlarmService;
import com.iv.bestalarmapp.ui.service.SoundService;

public class ChooseSoundActivity extends AbstractResourceActivity<Sound> {

	private SoundService soundService;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		soundService = new SoundService(this, new AlarmService(this));
	}

	public SourceType getSourceType() {
		return SourceType.SOUND;
	}

	public ListAdapter createResourceAdapter() {
		return new SoundListAdapter(this);
	}

	public void createResource(Alarm alarm) {
		Sound image;
		image = new Sound();
		image.mediaSourceId = getMediaSources().get(0).getId();
		alarm.sound = image;
	}

	@Override
	protected void mediaSourceUpdated(MediaSource currentMediaSource) {

	}

	@Override
	Sound getResource(Alarm alarm) {
		return alarm.sound;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		soundService.stop();
	}

	@Override
	void onSaveCustomFields(Sound selectedResource, Sound resource) {
		resource.isRingtone = selectedResource.isRingtone;
		resource.title = selectedResource.title;
		soundService.load(resource);
	}

}
