package com.iv.bestalarmapp.db;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.iv.bestalarmapp.ui.model.Alarm;
import com.iv.bestalarmapp.ui.model.Day;
import com.iv.bestalarmapp.ui.model.Image;
import com.iv.bestalarmapp.ui.model.ImageUri;
import com.iv.bestalarmapp.ui.model.Sound;
import com.iv.bestalarmapp.ui.model.SoundUri;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

/**
 * Database helper class used to manage the creation and upgrading of your
 * database. This class also usually provides the DAOs used by the other
 * classes.
 */
public class AlarmRepository extends OrmLiteSqliteOpenHelper {

	// name of the database file for your application -- change to something
	// appropriate for your app
	private static final String DATABASE_NAME = "bestalarmapp.db";
	// any time you make changes to your database objects, you may have to
	// increase the database version
	private static final int DATABASE_VERSION = 1;

	// the DAO object we use to access the SimpleData table
	private RuntimeExceptionDao<Alarm, Integer> alarmDao = null;
	private RuntimeExceptionDao<Day, Integer> dayDao;
	private RuntimeExceptionDao<Image, Integer> imageDao;
	private RuntimeExceptionDao<Sound, Integer> soundDao;
	private RuntimeExceptionDao<ImageUri, Integer> ImageUri;
	private RuntimeExceptionDao<SoundUri, Integer> soundUri;

	public AlarmRepository(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	/**
	 * This is called when the database is first created. Usually you should
	 * call createTable statements here to create the tables that will store
	 * your data.
	 */
	@Override
	public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
		try {
			Log.i(AlarmRepository.class.getName(), "onCreate");
			TableUtils.createTable(connectionSource, SoundUri.class);
			TableUtils.createTable(connectionSource, Sound.class);
			TableUtils.createTable(connectionSource, ImageUri.class);
			TableUtils.createTable(connectionSource, Image.class);
			TableUtils.createTable(connectionSource, Day.class);
			TableUtils.createTable(connectionSource, Alarm.class);
		} catch (SQLException e) {
			Log.e(AlarmRepository.class.getName(), "Can't create database", e);
			throw new RuntimeException(e);
		}

		// here we try inserting data in the on-create as a test
		RuntimeExceptionDao<Alarm, Integer> dao = getAlarmDao();
		// create some entries in the onCreate
		Alarm simple = new Alarm();
		simple.hour = 9;
		dao.create(simple);
		simple = new Alarm();
		simple.hour = 22;
		dao.create(simple);

	}

	/**
	 * This is called when your application is upgraded and it has a higher
	 * version number. This allows you to adjust the various data to match the
	 * new version number.
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource,
			int oldVersion, int newVersion) {
		try {
			Log.i(AlarmRepository.class.getName(), "onUpgrade");
			TableUtils.dropTable(connectionSource, Alarm.class, true);
			// after we drop the old databases, we create the new ones
			onCreate(db, connectionSource);
		} catch (SQLException e) {
			Log.e(AlarmRepository.class.getName(), "Can't drop databases", e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * Returns the Database Access Object (DAO) for our SimpleData class. It
	 * will create it or just give the cached value.
	 */
	public RuntimeExceptionDao<Alarm, Integer> getAlarmDao() {
		if (alarmDao == null) {
			alarmDao = getRuntimeExceptionDao(Alarm.class);
		}
		return alarmDao;
	}
	
	public RuntimeExceptionDao<Day, Integer> getDayDao() {
		if (dayDao == null) {
			dayDao = getRuntimeExceptionDao(Day.class);
		}
		return dayDao;
	}


	/**
	 * Close the database connections and clear any cached DAOs.
	 */
	@Override
	public void close() {
		super.close();
		alarmDao = null;
	}

	public RuntimeExceptionDao<Image, Integer> getImageDao() {
		if (imageDao == null) {
			imageDao = getRuntimeExceptionDao(Image.class);
		}
		return imageDao;
	}

	public RuntimeExceptionDao<Sound, Integer> getSoundDao() {
		if (soundDao == null) {
			soundDao = getRuntimeExceptionDao(Sound.class);
		}
		return soundDao;
	}
	
	public RuntimeExceptionDao<ImageUri, Integer> getImageUriDao() {
		if (ImageUri == null) {
			ImageUri = getRuntimeExceptionDao(ImageUri.class);
		}
		return ImageUri;
	}
	
	public RuntimeExceptionDao<SoundUri, Integer> getSoundUriDao() {
		if (soundUri == null) {
			soundUri = getRuntimeExceptionDao(SoundUri.class);
		}
		return soundUri;
	}
}
