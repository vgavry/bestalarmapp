package ua.net.imakhnyk.gae.bestalarm;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import org.json.simple.parser.JSONParser;


public class UrlFetchService {
	public static String getString(String link) throws IOException {
		URL url = new URL(link);
		InputStream openStream = url.openStream();
		String result = readFully(openStream,"UTF-8");
		openStream.close();
		return result.toString();
	}
	public static Object getJson(String link) throws Exception {
			URL url = new URL(link);
			Object result = (new JSONParser()).parse(new InputStreamReader(url.openStream()));
			return result;
	}
	
	public static String readFully(InputStream inputStream, String encoding)
	        throws IOException {
	    return new String(readFully(inputStream), encoding);
	}    

	private static byte[] readFully(InputStream inputStream)
	        throws IOException {
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    byte[] buffer = new byte[1024];
	    int length = 0;
	    while ((length = inputStream.read(buffer)) != -1) {
	        baos.write(buffer, 0, length);
	    }
	    return baos.toByteArray();
	}
}
