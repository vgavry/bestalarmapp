package ua.net.imakhnyk.gae.bestalarm;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.http.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

@SuppressWarnings("serial")
public class FindSCMusicServlet extends HttpServlet {

	private static final String SC_KEY = "059a55522d7a12411363c756874c1afd";

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		String keyword = req.getParameter("k");
		JSONObject response = new JSONObject();
		try {
			JSONArray json = (JSONArray) UrlFetchService
					.getJson("https://api.soundcloud.com/tracks.json?client_id="
							+ SC_KEY + "&q=" + URLEncoder.encode(keyword,"UTF-8"));
			JSONArray musics = new JSONArray();
			response.put("records", musics);
			for (int i = 0; i < json.size(); i++) {
				JSONObject scRecord = (JSONObject) json.get(i);
				String title = (String) scRecord.get("title");
				String download = (String) scRecord.get("download_url");
				String stream = (String) scRecord.get("stream_url");
				String icon = (String) scRecord.get("artwork_url");
				

				if ((download != null) || (stream != null)) {
					JSONObject photo = new JSONObject();
					photo.put("title", title);
					photo.put("icon", icon);
					if (download != null) {
						photo.put("url", download + "?client_id=" + SC_KEY);
					}
					if (stream != null) {
						photo.put("stream", stream + "?client_id=" + SC_KEY);
					}
					musics.add(photo);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.put("status", "filed");
			response.put("error", e.getMessage());
		}
		resp.setContentType("application/json");
		resp.getWriter().print(response.toString());
	}
}
