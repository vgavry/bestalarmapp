package ua.net.imakhnyk.gae.bestalarm;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.http.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

@SuppressWarnings("serial")
public class FindFlickrImagesServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		String keyword = req.getParameter("k");
		JSONObject response = new JSONObject();
		try {
			String apiKey = "4b3a9c96c4fb20ac953a77edb1f72fd1";
			ApiSigGenerator asg = new ApiSigGenerator("11e3c45af45c459c"); 
			String apiSig= asg.sign("method","flickr.photos.search","api_key",apiKey,"text", keyword,"format","json","nojsoncallback","1");
			JSONObject json = (JSONObject) UrlFetchService.getJson("https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key="+apiKey+"&text="+URLEncoder.encode(keyword,"UTF-8")+"&format=json&nojsoncallback=1&api_sig="+apiSig);
			System.out.println(json);
			JSONArray photos = new JSONArray();
			response.put("photos", photos);
			JSONArray flickrPhotos = (JSONArray) ((JSONObject)json.get("photos")).get("photo");
			for(int i=0;i<flickrPhotos.size();i++){
				JSONObject fPhoto = (JSONObject) flickrPhotos.get(i);
				String server = (String) fPhoto.get("server");
				String id = (String) fPhoto.get("id");
				String secret = (String) fPhoto.get("secret");
				String title = (String) fPhoto.get("title");
				JSONObject photo = new JSONObject();
				photo.put("title", title);
				photo.put("url", "http://c4.staticflickr.com/4/"+server+"/"+id+"_"+secret+"_c.jpg");
				photos.add(photo);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.put("status", "filed");
			response.put("error", e.getMessage());
		}
		resp.setContentType("application/json");
		resp.getWriter().print(response.toString());
	}
}
